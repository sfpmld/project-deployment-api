ARG NODE_VERSION=16.14.2

# ----- Dependencies -----
FROM node:$NODE_VERSION-alpine AS dependencies
WORKDIR /app
COPY package*.json ./
COPY src src

# Install minimal production packages
RUN npm i --only=production --ignore-scripts

# ----- Build -----
FROM dependencies AS build
WORKDIR /app
COPY .eslintrc.js tsconfig*.json ./
COPY testSettings testSettings

# Install dev dependencies required to build
RUN npm i --only=dev && \
    npm cache clean --force && \
    # Run build
    npm run build

# ----- Release with Alpine ------
FROM node:$NODE_VERSION-alpine AS release
WORKDIR /app

# Install in production mode
COPY --from=dependencies /app/node_modules node_modules
# Copy required files
COPY --from=build /app/dist ./dist

COPY package.json ./
# Start
CMD ["npm", "run", "start"]

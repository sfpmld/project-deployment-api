@PHONY: test-unit test-spec start

test-unit:
	@echo "Running unit tests"
	@DO='npm run test:unit:watch' docker compose -f ./testSettings/docker/docker-compose.test.yml up

test-spec:
	@echo "Running system tests"
	@DO='npm run test:spec:watch' docker compose -f ./testSettings/docker/docker-compose.test.yml  up

start:
	@echo "Starting the application"
	@DO='npm run start' docker compose -f ./testSettings/docker/docker-compose.test.yml up

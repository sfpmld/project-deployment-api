-- Database: project_deploy

-- DROP DATABASE IF EXISTS project_deploy;
-- CREATE DATABASE IF NOT EXISTS project_deploy;

CREATE TABLE IF NOT EXISTS "users" (
  "id" SERIAL PRIMARY KEY,
  "username" TEXT NOT NULL,
  "email" TEXT NOT NULL,
  "created_at" TIMESTAMP NOT NULL
);
CREATE TABLE IF NOT EXISTS "projects" (
  "id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL,
  "url" TEXT,
  "user_id" INT NOT NULL REFERENCES "users"("id"),
  "app_secret" TEXT NOT NULL,
  "created_at" TIMESTAMP NOT NULL
);
CREATE TABLE IF NOT EXISTS "deployments" (
  "id" SERIAL PRIMARY KEY,
  "deployed_in" NUMERIC,
  "status" TEXT DEFAULT 'pending' CHECK (status = ANY (ARRAY['pending', 'building', 'deploying', 'done', 'failed', 'cancelled'])),
  "project_id" INT NOT NULL REFERENCES "projects"("id"),
  "created_at" TIMESTAMP NOT NULL
);

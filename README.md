# Project Deployment API

## Introduction
This project has been made with a focus on clean and hexagonal architecture. The patterns and principles primarily draw inspiration from Tom Homberg's book "Get Your Hands Dirty With Clean Architecture."
You'll notice that I've used different in/out model (contract) or interfaces or repository by usecase. The reason is also from the book, keeping them separate enhance Single Responsability principle,
I think. The more maintenability even if there are more file.
This is not how we do at my worksplace. But at this time this is what I read. It could change maybe in 2 month but at this time, this what I like to read. So my test assignments are always impacted :)

## Architecture
The project is designed with the aspiration to follow clean and hexagonal architectural patterns. Meaning trying to apply DI at maximum.
I have then made some radical choices like slicing interfaces for each usecase. This is also inspired from the book mentionned above.
I have experienced a real improvement in Interface Segregation Principle, Libskov Substitution Principle and Segration Principle application and then benefits.

### Highlights:
- Application of the Dependency Inversion Principle
- Dependency Injection

## Database Schema Enhancement
In a strategic move to facilitate actions like searching the first deployment or authorization purposes, a `project_id` field has been added to the `deployment` table.

## Getting Started
Follow the installation steps to get started.

# Unit Test
npm run test:unit:watch or make test:unit:watch

# System Test
make test:spec:watch

# All Test
npm run test:watch


# Install dependencies
npm install

# Run the application
npm start



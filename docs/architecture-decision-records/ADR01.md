__ADR: Initial Core Architecture__

__Context__
There seems to be 2 subdomain entities i.e project and deployment and a suporting subdomain data-aggretion

__Decision__
We will start with an architecture based on the Hexagonal Architecture to be open to every learning insight
while evolving in this services building. As this kind of architecture let us delay infrastructure question for later.
So this should facilitate iteration on learning domain insights

__Status__
Accepted

__Consequences__
We will be able to start and iterating with test suite support

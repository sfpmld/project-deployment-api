import DeploymentStatuses from '../../../__lib/sharedKernel/deployment/DeploymentStatus';

export type DeploymentDbProps = {
  id: number;
  deployed_in: number | null;
  status: DeploymentStatuses;
  created_at: Date;
  project_id: number;
};

export type Deployment = {
  id: number;
  deployedIn: number | null;
  status: DeploymentStatuses;
  createdAt: string;
  projectId: number;
};

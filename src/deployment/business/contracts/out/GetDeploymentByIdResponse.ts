import { Deployment } from '../../entities/deployment';

export type GetDeploymentByIdResponse = Deployment | undefined;

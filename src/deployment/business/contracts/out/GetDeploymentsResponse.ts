import { Deployment } from '../../../../project/business/entities/deployment';

export type GetDeploymentsResponse = { data: Deployment[]; total: number };

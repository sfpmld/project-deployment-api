import DeploymentStatuses from '../../../../__lib/sharedKernel/deployment/DeploymentStatus';

export type DeploymentStatusChangedMessage = {
  message_id: string;
  project_id: number;
  deployment_id: number;
  status: DeploymentStatuses;
  date: Date;
};

import { Deployment } from '../../entities/deployment';

export type DeleteDeploymentResponse = Deployment;

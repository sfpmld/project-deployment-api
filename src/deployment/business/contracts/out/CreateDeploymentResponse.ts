import { Deployment } from '../../entities/deployment';

export type CreateDeploymentResponse = Deployment;

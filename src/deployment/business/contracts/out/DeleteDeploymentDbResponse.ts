import { DeploymentDbProps } from '../../entities/deployment';

export type DeleteDeploymentDbResponse = DeploymentDbProps;

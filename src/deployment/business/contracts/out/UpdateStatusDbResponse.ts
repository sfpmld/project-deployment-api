import { DeploymentDbProps } from '../../entities/deployment';

export type UpdateDeploymentStatusDbResponse = DeploymentDbProps;

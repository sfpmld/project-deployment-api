import { DeploymentDbProps } from '../../entities/deployment';

export type GetDeploymentByIdDbResponse = DeploymentDbProps;

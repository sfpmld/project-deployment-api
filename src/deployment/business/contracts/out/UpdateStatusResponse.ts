import { Deployment } from '../../../../project/business/entities/deployment';

export type UpdateDeploymentStatusResponse = Deployment | undefined;

import DeploymentStatuses from '../../../../__lib/sharedKernel/deployment/DeploymentStatus';

export type DeploymentDbProps = {
  id: number;
  deployed_in: number | null;
  status: DeploymentStatuses;
  created_at: Date;
  project_id: number;
};

export type GetDeploymentsDbResponse = { data: DeploymentDbProps[]; total: number };

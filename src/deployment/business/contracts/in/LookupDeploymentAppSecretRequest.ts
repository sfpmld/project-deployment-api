export type LookupDeploymentAppSecretRequest = {
  id: number;
  bearerAgainstAppSecret: string;
};

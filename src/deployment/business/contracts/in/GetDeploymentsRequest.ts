export type GetDeploymentsRequest = {
  page: number;
  limit?: number;
};

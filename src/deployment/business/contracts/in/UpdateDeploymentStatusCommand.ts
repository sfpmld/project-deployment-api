import DeploymentStatuses from '../../../../__lib/sharedKernel/deployment/DeploymentStatus';

export type UpdateDeploymentStatusCommand = {
  id: number;
  status: DeploymentStatuses;
};

export type UpdateDeploymentStatusDbCommand = {
  id: number;
  status: DeploymentStatuses;
  deployed_in?: number;
};

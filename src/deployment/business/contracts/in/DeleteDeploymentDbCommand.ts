import DeploymentStatuses from '../../../../__lib/sharedKernel/deployment/DeploymentStatus';

export type DeleteDeploymentDbCommand = { id: number; status: DeploymentStatuses };

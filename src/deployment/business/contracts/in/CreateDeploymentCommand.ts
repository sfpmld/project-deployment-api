import DeploymentStatuses from '../../../../__lib/sharedKernel/deployment/DeploymentStatus';

export type CreateDeploymentCommand = number;

export type CreateDeploymentDbCommand = {
  deployed_in: number | null;
  status: DeploymentStatuses;
  created_at: string;
  project_id: number;
};

import { CreateDeploymentDbCommand } from '../business/contracts/in/CreateDeploymentCommand';
import { GetDeploymentsRequest } from '../business/contracts/in/GetDeploymentsRequest';
import { GetDeploymentsDbResponse } from '../business/contracts/out/GetDeploymentsDbResponse';
import deploymentFixtures from '../../../testSettings/fixtures/deployments.json';
import paginationService from '../../infrastructure/database/sql/pagination';
import { GetDeploymentByIdDbResponse } from '../business/contracts/out/GetDeploymentByIdDbResponse';
import DeploymentStatuses from '../../__lib/sharedKernel/deployment/DeploymentStatus';
import { DeleteDeploymentDbResponse } from '../business/contracts/out';
import {
  UpdateDeploymentStatusCommand,
  UpdateDeploymentStatusDbCommand,
} from '../business/contracts/in/UpdateDeploymentStatusCommand';
import { GetDeploymentByIdRequest } from '../business/contracts/in/GetDeploymentByIdRequest';
import { DeleteDeploymentDbCommand } from '../business/contracts/in/DeleteDeploymentDbCommand';
import { DeploymentDbProps } from '../business/entities/deployment';
import { CombinedDeploymentRepositoriesInterfaces } from './index';
import { UpdateDeploymentStatusDbResponse } from '../business/contracts/out/UpdateStatusDbResponse';

type DeploymentFixturesJsonFormat = {
  id: number;
  deployed_in: number | null;
  status: DeploymentStatuses;
  created_at: string;
  project_id: number;
};

class DeploymentRepositorySpy implements CombinedDeploymentRepositoriesInterfaces {
  createSpy: CreateDeploymentDbCommand;

  updateSpy: UpdateDeploymentStatusDbCommand;

  deleteSpy: DeleteDeploymentDbCommand;

  primerDeploymentInit = false;

  public initPrimerDeployment(value: boolean): void {
    this.primerDeploymentInit = value;
  }

  public async create(command: CreateDeploymentDbCommand): Promise<DeploymentDbProps> {
    this.createSpy = command;
    return Promise.resolve({
      id: 1,
      deployed_in: null,
      status: DeploymentStatuses.PENDING,
      created_at: new Date(),
      project_id: 1,
    });
  }

  public async getDeployments(request: GetDeploymentsRequest): Promise<GetDeploymentsDbResponse> {
    const deployments = await Promise.resolve(
      deploymentFixtures as unknown as DeploymentFixturesJsonFormat[]
    );
    const { limit } = paginationService(request.page, request.limit);
    const paginatedDeployments = deployments.slice(0, limit);
    const data = this.processDateFromJsonSample(paginatedDeployments);
    const count = data.length;
    return { data, total: count };
  }

  public async getDeploymentById(
    id: GetDeploymentByIdRequest
  ): Promise<GetDeploymentByIdDbResponse | undefined> {
    const deployments = deploymentFixtures as DeploymentFixturesJsonFormat[];

    const searchedDeployment = deployments.find(
      (dep) => dep.id === id
    ) as DeploymentFixturesJsonFormat;
    if (!searchedDeployment) {
      return undefined;
    }
    const deployment = this.processDateFromJsonSample([searchedDeployment])[0];

    return deployment;
  }

  public async updateStatus(
    command: UpdateDeploymentStatusDbCommand
  ): Promise<UpdateDeploymentStatusDbResponse> {
    console.log(`Updating deployment status to "${command.status}"...`);
    const toUpdate = deploymentFixtures.find(
      (dep) => dep.id === command.id
    ) as DeploymentFixturesJsonFormat;
    this.updateSpy = command;

    return Promise.resolve(this.processDateFromJsonSample([toUpdate])[0]);
  }

  public async isPrimerDeployment(request: UpdateDeploymentStatusCommand): Promise<boolean> {
    console.log(
      `Checking if deployment is primer with id "${request.id}"... => ${this.primerDeploymentInit}`
    );
    return Promise.resolve(this.primerDeploymentInit);
  }

  public async softDelete(command: DeleteDeploymentDbCommand): Promise<DeleteDeploymentDbResponse> {
    this.deleteSpy = command;
    console.log(`Deleting deployment with id "${command}"...`);

    const toDelete = deploymentFixtures.find(
      (dep) => dep.id === command.id
    ) as DeploymentFixturesJsonFormat;
    if (!toDelete) {
      throw new Error(`Deployment with id "${command.id}" not found`);
    }
    const result = this.processDateFromJsonSample([toDelete])[0];
    return Promise.resolve(result);
  }

  // PRIVATE METHODS
  private processDateFromJsonSample(
    deployments: DeploymentFixturesJsonFormat[]
  ): DeploymentDbProps[] {
    return deployments.map((deployment) => ({
      // eslint-disable-next-line
      ...deployment,
      created_at: new Date(deployment.created_at),
    }));
  }
}

export default DeploymentRepositorySpy;

import { CreateDeploymentRepositoryInterface } from '../application/interfaces/out/CreateDeployment.repository';
import { DeleteDeploymentRepositoryInterface } from '../application/interfaces/out/DeleteDeployment.repository';
import { GetDeploymentsRepositoryInterface } from '../application/interfaces/out/GetDeployment.repository';
import { GetDeploymentByIdRepositoryInterface } from '../application/interfaces/out/GetDeploymentById.repository';
import { UpdateDeploymentStatusRepositoryInterface } from '../application/interfaces/out/UpdateDeploymentStatus.repository';

export interface CombinedDeploymentRepositoriesInterfaces
  extends CreateDeploymentRepositoryInterface,
    GetDeploymentsRepositoryInterface,
    GetDeploymentByIdRepositoryInterface,
    UpdateDeploymentStatusRepositoryInterface,
    DeleteDeploymentRepositoryInterface {}

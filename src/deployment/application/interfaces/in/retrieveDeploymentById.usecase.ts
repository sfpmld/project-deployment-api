import { UsecaseInterface } from '../../../../__lib/core/usecase.interface';
import { GetDeploymentByIdRequest } from '../../../business/contracts/in';
import { GetDeploymentByIdResponse } from '../../../business/contracts/out';

export interface GetDeploymentByIdUseCaseInterface
  extends UsecaseInterface<GetDeploymentByIdRequest, GetDeploymentByIdResponse> {}

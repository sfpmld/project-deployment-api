import { UsecaseInterface } from '../../../../__lib/core/usecase.interface';
import { UpdateDeploymentStatusCommand } from '../../../business/contracts/in';
import { UpdateDeploymentStatusResponse } from '../../../business/contracts/out';

export interface UpdateDeploymentStatusUseCaseInterface
  extends UsecaseInterface<UpdateDeploymentStatusCommand, UpdateDeploymentStatusResponse> {}

export default UpdateDeploymentStatusUseCaseInterface;

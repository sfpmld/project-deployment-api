import { UsecaseInterface } from '../../../../__lib/core/usecase.interface';
import { DeleteDeploymentCommand } from '../../../business/contracts/in';
import { DeleteDeploymentResponse } from '../../../business/contracts/out';

export interface DeleteDeploymentUseCaseInterface
  extends UsecaseInterface<DeleteDeploymentCommand, DeleteDeploymentResponse> {}

export default DeleteDeploymentUseCaseInterface;

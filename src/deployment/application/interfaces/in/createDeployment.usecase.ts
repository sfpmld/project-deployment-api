import { UsecaseInterface } from '../../../../__lib/core/usecase.interface';
import { CreateDeploymentCommand } from '../../../business/contracts/in';
import { CreateDeploymentResponse } from '../../../business/contracts/out';

export interface CreateDeploymentUseCaseInterface
  extends UsecaseInterface<CreateDeploymentCommand, CreateDeploymentResponse> {}


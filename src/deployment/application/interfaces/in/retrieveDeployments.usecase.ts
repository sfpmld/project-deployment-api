import { UsecaseInterface } from '../../../../__lib/core/usecase.interface';
import { GetDeploymentsRequest } from '../../../business/contracts/in';
import { GetDeploymentsResponse } from '../../../business/contracts/out';

export interface GetDeploymentsUseCaseInterface
  extends UsecaseInterface<GetDeploymentsRequest, GetDeploymentsResponse> {}

export default GetDeploymentsUseCaseInterface;

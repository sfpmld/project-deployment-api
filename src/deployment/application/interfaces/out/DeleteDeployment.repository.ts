import { DeleteDeploymentDbCommand } from '../../../business/contracts/in';
import { DeleteDeploymentDbResponse } from '../../../business/contracts/out';

export interface DeleteDeploymentRepositoryInterface {
  softDelete: (command: DeleteDeploymentDbCommand) => Promise<DeleteDeploymentDbResponse>;
}

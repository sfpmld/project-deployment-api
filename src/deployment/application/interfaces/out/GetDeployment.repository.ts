import { GetDeploymentsRequest } from '../../../business/contracts/in';
import { GetDeploymentsDbResponse } from '../../../business/contracts/out';

export interface GetDeploymentsRepositoryInterface {
  getDeployments: (request: GetDeploymentsRequest) => Promise<GetDeploymentsDbResponse>;
}

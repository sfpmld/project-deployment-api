import { LookupDeploymentAppSecretRequest } from '../../../business/contracts/in';
import { LookupDeploymentAppSecretResponse } from '../../../business/contracts/out';

export interface LookupDeploymentAppSecretRepositoryInterface {
  lookupAppSecret(
    request: LookupDeploymentAppSecretRequest
  ): Promise<LookupDeploymentAppSecretResponse>;
}

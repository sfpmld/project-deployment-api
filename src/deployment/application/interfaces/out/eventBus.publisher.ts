import { DeploymentStatusChangedMessage } from '../../../business/contracts/out';

export interface EventBusInterface {
  publish: (message: DeploymentStatusChangedMessage) => Promise<void>;
}

export default EventBusInterface;

import { CreateDeploymentDbCommand } from '../../../business/contracts/in';
import { DeploymentDbProps } from '../../../business/entities/deployment';

export interface CreateDeploymentRepositoryInterface {
  create: (request: CreateDeploymentDbCommand) => Promise<DeploymentDbProps>;
}

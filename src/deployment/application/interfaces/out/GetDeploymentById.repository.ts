import { GetDeploymentByIdRequest } from '../../../../project/business/contracts/in';
import { GetDeploymentByIdDbResponse } from '../../../business/contracts/out';

export interface GetDeploymentByIdRepositoryInterface {
  getDeploymentById: (
    request: GetDeploymentByIdRequest
  ) => Promise<GetDeploymentByIdDbResponse | undefined>;
}

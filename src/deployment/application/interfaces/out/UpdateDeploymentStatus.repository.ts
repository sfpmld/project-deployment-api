import {
  UpdateDeploymentStatusCommand,
  UpdateDeploymentStatusDbCommand,
} from '../../../business/contracts/in';
import { UpdateDeploymentStatusDbResponse } from '../../../business/contracts/out/UpdateStatusDbResponse';
import { DeploymentDbProps } from '../../../business/entities/deployment';

export interface UpdateDeploymentStatusRepositoryInterface {
  getDeploymentById: (
    id: UpdateDeploymentStatusCommand['id']
  ) => Promise<DeploymentDbProps | undefined>;
  updateStatus: (
    command: UpdateDeploymentStatusDbCommand
  ) => Promise<UpdateDeploymentStatusDbResponse>;
  isPrimerDeployment: (request: UpdateDeploymentStatusCommand) => Promise<boolean>;
}

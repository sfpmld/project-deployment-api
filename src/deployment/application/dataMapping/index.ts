import { v4 as uuidv4 } from 'uuid';
import { DateProviderInterface } from '../../../__lib/core/dateProvider';
import DeploymentStatuses from '../../../__lib/sharedKernel/deployment/DeploymentStatus';
import { CreateDeploymentCommand, CreateDeploymentDbCommand } from '../../business/contracts/in';
import { DeploymentStatusChangedMessage } from '../../business/contracts/out';
import { Deployment, DeploymentDbProps } from '../../business/entities/deployment';

type MessagePayload = {
  id: number;
  deployed_in: number | null;
  status: DeploymentStatuses;
  created_at: Date;
  project_id: number;
};

const dataMapper = {
  toDb: (data: CreateDeploymentCommand): CreateDeploymentDbCommand => ({
    deployed_in: null,
    status: DeploymentStatuses.PENDING,
    created_at: new Date().toISOString(),
    project_id: data,
  }),

  toView(data: DeploymentDbProps[]): Deployment[] {
    return data.map((deployment: DeploymentDbProps) => ({
      id: deployment.id,
      deployedIn: deployment.deployed_in,
      status: deployment.status,
      createdAt: deployment.created_at.toISOString(),
      projectId: deployment.project_id,
    }));
  },

  toMessage:
    (dateProvider: DateProviderInterface) =>
    (data: MessagePayload): DeploymentStatusChangedMessage => ({
      message_id: uuidv4(),
      project_id: data.project_id,
      deployment_id: data.id,
      status: data.status,
      date: dateProvider.now(),
    }),
};

export default dataMapper;

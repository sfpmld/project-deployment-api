import { CreateProjectDeploymentCommand } from '../../../project/business/contracts/in';
import { CreateProjectDeploymentServiceResponse } from '../../../project/business/contracts/out';
import { CreateDeploymentRepositoryInterface, EventBusInterface } from '../interfaces/out';
import { CreateProjectDeploymentServiceInterface } from '../../../project/application/interfaces/out';
import makeCreateDeploymentUseCase from '../usecases/CreateDeploymentUseCase';
import { DateProviderInterface } from '../../../__lib/core/dateProvider';

const makeDeploymentCreationServiceForExternal = (
  repository: CreateDeploymentRepositoryInterface,
  eventBus: EventBusInterface,
  dateProvider: DateProviderInterface
): CreateProjectDeploymentServiceInterface => {
  return {
    async create(
      command: CreateProjectDeploymentCommand
    ): Promise<CreateProjectDeploymentServiceResponse> {
      const deploymentUseCase = makeCreateDeploymentUseCase(repository, eventBus, dateProvider);

      const createdDeployment = await deploymentUseCase.handle(command);

      return createdDeployment;
    },
  };
};

export default makeDeploymentCreationServiceForExternal;

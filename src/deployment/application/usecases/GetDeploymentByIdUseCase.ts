import { GetDeploymentByIdRequest } from '../../business/contracts/in';
import { GetDeploymentByIdResponse } from '../../business/contracts/out/GetDeploymentByIdResponse';
import dataMapper from '../dataMapping/index';
import { GetDeploymentByIdRepositoryInterface } from '../interfaces/out';

const GetDeploymentByIdUseCase = (repository: GetDeploymentByIdRepositoryInterface) => ({
  async handle(request: GetDeploymentByIdRequest): Promise<GetDeploymentByIdResponse> {
    const deployment = await repository.getDeploymentById(request);
    if (!deployment) {
      return undefined;
    }
    const result = dataMapper.toView([deployment]);

    return result[0];
  },
});

export default GetDeploymentByIdUseCase;

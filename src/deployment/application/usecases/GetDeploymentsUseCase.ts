import dataMapper from '../dataMapping/index';
import { GetDeploymentsRequest } from '../../business/contracts/in';
import { GetDeploymentsResponse } from '../../business/contracts/out';
import { GetDeploymentsUseCaseInterface } from '../interfaces/in';
import { GetDeploymentsRepositoryInterface } from '../interfaces/out';

const GetDeploymentsUseCase = (
  repository: GetDeploymentsRepositoryInterface
): GetDeploymentsUseCaseInterface => ({
  async handle(request: GetDeploymentsRequest): Promise<GetDeploymentsResponse> {
    const deployments = await repository.getDeployments(request);

    const result = dataMapper.toView(deployments.data);

    return { data: result, total: deployments.total };
  },
});

export default GetDeploymentsUseCase;

import DEFAULT_PAGINATION_LIMIT from '../../../../__config/pagination';
import { GetDeploymentsRequest } from '../../../business/contracts/in';
import { GetDeploymentsResponse } from '../../../business/contracts/out';
import { GetDeploymentsUseCaseInterface } from '../../interfaces/in';
import DeploymentRepositorySpy from '../../../repository/deploymentRepositorySpy';
import GetDeploymentsUseCase from '../GetDeploymentsUseCase';

class SUT {
  private sut: GetDeploymentsUseCaseInterface;

  private result: GetDeploymentsResponse;

  constructor(private testing: typeof GetDeploymentsUseCase) {}

  setupWith(deploymentRepository: DeploymentRepositorySpy) {
    this.sut = this.testing(deploymentRepository);
    return this;
  }

  async handleRequest(request: GetDeploymentsRequest) {
    this.result = await this.sut.handle(request);
  }

  processShouldSucceed() {
    expect(this.result.data).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          id: expect.any(Number),
          deployedIn: null,
          status: expect.any(String),
          createdAt: expect.any(String),
        }),
      ])
    );
  }

  resultShouldReturnLengthOf(length: number) {
    expect(this.result.data.length).toEqual(length);
  }
}

let sut: SUT;
beforeAll(async () => {
  sut = new SUT(GetDeploymentsUseCase).setupWith(new DeploymentRepositorySpy());
});
describe('Story: User is able to Retrieve all existing deployment with paginated results', () => {
  describe('Given incoming request to GET all existing deployments', () => {
    describe('When handled by get deployments usecase', () => {
      it('Then it should succeed', async () => {
        const request: GetDeploymentsRequest = {
          page: 1,
        };

        await sut.handleRequest(request);

        sut.processShouldSucceed();
      });

      it('And it should return a list of deployments with pagination', async () => {
        const request: GetDeploymentsRequest = {
          page: 1,
        };

        await sut.handleRequest(request);

        sut.resultShouldReturnLengthOf(DEFAULT_PAGINATION_LIMIT);
      });
    });
  });
});

import ProjectRepositorySpy from '../../../../project/repository/projectRepositorySpy';
import DeploymentStatuses from '../../../../__lib/sharedKernel/deployment/DeploymentStatus';
import { UrlRandomizerInterface } from '../../../../__lib/utils/urlRandomizer';
import urlRandomizerStub, { urlRandomizedDummy } from '../../../../__lib/utils/urlRandomizerStub';
import { UpdateDeploymentStatusCommand } from '../../../business/contracts/in';
import { UpdateDeploymentStatusResponse } from '../../../business/contracts/out';
import { UpdateDeploymentStatusUseCaseInterface } from '../../interfaces/in';
import DeploymentRepositorySpy from '../../../repository/deploymentRepositorySpy';
import UpdateDeploymentStatusUseCase from '../UpdateDeploymentStatusUseCase';
import generatefakeDoneDeploymentDateISOString from '../../../../../testSettings/testHelpers/utils';
import { DateProviderInterface, DateProviderStub } from '../../../../__lib/core/dateProvider';
import EventBusPublisherSpy from '../../../../infrastructure/message-gateway/eventBusPublisherSpy';
import deploymentDummies from '../../../../../testSettings/fixtures/deployments.json';

const deploymentDummy = deploymentDummies[0];

class SUT {
  private sut: UpdateDeploymentStatusUseCaseInterface;

  private result: UpdateDeploymentStatusResponse;

  private deploymentRepository: DeploymentRepositorySpy;

  private projectRepository: ProjectRepositorySpy;

  private eventBus: EventBusPublisherSpy;

  constructor(private testing: typeof UpdateDeploymentStatusUseCase) {}

  setupWith(
    deploymentRepository: DeploymentRepositorySpy,
    projectRepository: ProjectRepositorySpy,
    eventBus: EventBusPublisherSpy,
    urlRandomizer: UrlRandomizerInterface,
    dateProvider: DateProviderInterface
  ) {
    this.deploymentRepository = deploymentRepository;
    this.projectRepository = projectRepository;
    this.eventBus = eventBus;
    this.sut = this.testing(
      deploymentRepository,
      projectRepository,
      eventBus,
      urlRandomizer,
      dateProvider
    );
    return this;
  }

  public initPrimerDeploymentReturnValueTo(value: boolean) {
    this.deploymentRepository.initPrimerDeployment(value);
  }

  public async handleCommand(command: UpdateDeploymentStatusCommand) {
    this.result = await this.sut.handle(command);
  }

  public processShouldSucceed(): void {
    expect(this.result).toEqual(
      expect.objectContaining({
        id: expect.any(Number),
        // deployed_in: expect.any(Number),
        status: expect.any(String),
        createdAt: expect.any(String),
        projectId: expect.any(Number),
      })
    );
  }

  public shouldHaveUpdatedStatusTo(status: DeploymentStatuses): void {
    expect(this.deploymentRepository.updateSpy.status).toBe(status);
  }

  public projectUrlShouldHaveBeenUpdatedWith(url: string): void {
    expect(this.projectRepository.updateUrlSpy.url).toBe(url);
  }

  public shouldHaveUpdatedDeployedInTimeTo(time: number): void {
    expect(this.deploymentRepository.updateSpy.deployed_in).toBe(time);
  }

  public shouldNOTHaveUpdatedDeployedInTimeTo(time: number): void {
    expect(this.deploymentRepository.updateSpy.deployed_in).not.toBe(time);
    expect(this.deploymentRepository.updateSpy.deployed_in).toBeFalsy();
  }

  deploymentCreationMessageShouldHaveBeenPublished() {
    expect(this.eventBus.eventsSpy.length).toBeGreaterThan(0);
  }
}

let sut: SUT;
const dateProviderStub = new DateProviderStub();
beforeEach(async () => {
  sut = new SUT(UpdateDeploymentStatusUseCase).setupWith(
    new DeploymentRepositorySpy(),
    new ProjectRepositorySpy(),
    new EventBusPublisherSpy(),
    urlRandomizerStub,
    dateProviderStub
  );
});
describe('Story: Webhooks are able to udpate the status of a given deployment', () => {
  describe('Given incoming command to update a given deployment status', () => {
    describe('When handled by update status usecase', () => {
      it('Then it should succeed', async () => {
        const command = {
          id: 1,
          status: DeploymentStatuses.DEPLOYING,
          project_id: 1,
        };

        await sut.handleCommand(command);

        sut.processShouldSucceed();
      });

      it('And a message should have been published to external service (like data-aggregator)', async () => {
        // Arrange
        const command = {
          id: 1,
          status: DeploymentStatuses.DEPLOYING,
          project_id: 1,
        };

        await sut.handleCommand(command);

        sut.deploymentCreationMessageShouldHaveBeenPublished();
      });
    });
  });

  describe('Rule: When an update arrives with the "done" status, the "deployed_in" property on the deployment should be filled with the time it took (in seconds) to go from "pending" to "done";', () => {
    describe(`Given incoming command to update a given deployment status to ${DeploymentStatuses.DONE}`, () => {
      describe('When handled by update usecase', () => {
        it('Then deployed_in should be udpated to corresonding time elapsed', async () => {
          // Arrange
          const command = {
            id: 1,
            project_id: 1,
            status: DeploymentStatuses.DONE,
          };
          const waitingTimeInSec = 10;
          const deploymentDateCreation = new Date(deploymentDummy.created_at);
          const fakeDoneDate = generatefakeDoneDeploymentDateISOString(
            deploymentDateCreation,
            waitingTimeInSec
          );
          dateProviderStub.setInitDateISOString(fakeDoneDate);

          // Act
          await sut.handleCommand(command);

          // Assert
          sut.shouldHaveUpdatedDeployedInTimeTo(waitingTimeInSec);
        });
      });
    });

    describe(`Given incoming command to update a given deployment status OTHER THAN ${DeploymentStatuses.DONE}`, () => {
      describe('When handled by update usecase', () => {
        it('Then deployed_in should be udpated to corresonding time elapsed', async () => {
          // Arrange
          const command = {
            id: 1,
            project_id: 1,
            status: DeploymentStatuses.BUILDING,
          };
          const waitingTimeInSec = 10;
          const deploymentDateCreation = new Date(deploymentDummy.created_at);
          const fakeDoneDate = generatefakeDoneDeploymentDateISOString(
            deploymentDateCreation,
            waitingTimeInSec
          );
          dateProviderStub.setInitDateISOString(fakeDoneDate);

          // Act
          await sut.handleCommand(command);

          // Assert
          sut.shouldNOTHaveUpdatedDeployedInTimeTo(waitingTimeInSec);
        });
      });
    });
  });

  describe(`Rule: When incoming done status (${DeploymentStatuses.DONE}) arrives for the first deployment of the project`, () => {
    describe(`Given incoming command to update a given deployment status to ${DeploymentStatuses.DONE}`, () => {
      describe('When handled by update status usecase', () => {
        it(`Then it should update the project status to ${DeploymentStatuses.DONE}`, async () => {
          // Arrange
          const command = {
            id: 1,
            project_id: 1,
            status: DeploymentStatuses.DONE,
          };

          // Act
          await sut.handleCommand(command);

          // Assert
          sut.shouldHaveUpdatedStatusTo(DeploymentStatuses.DONE);
        });

        it('And a the URL of the related project should have been update with a randomly generated URL', async () => {
          // Arrange
          const expectedUrl = urlRandomizedDummy;
          const command = {
            id: 1,
            project_id: 1,
            status: DeploymentStatuses.DONE,
          };
          sut.initPrimerDeploymentReturnValueTo(true);

          // Act
          await sut.handleCommand(command);

          // Assert
          sut.projectUrlShouldHaveBeenUpdatedWith(expectedUrl);
        });
      });
    });
  });
});

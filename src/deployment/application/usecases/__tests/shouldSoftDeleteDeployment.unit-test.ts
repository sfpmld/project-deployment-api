import { DateProviderInterface, DateProviderStub } from '../../../../__lib/core/dateProvider';
import DeploymentStatuses from '../../../../__lib/sharedKernel/deployment/DeploymentStatus';
import { DeleteDeploymentCommand } from '../../../business/contracts/in/DeleteDeploymentCommand';
import { DeleteDeploymentResponse } from '../../../business/contracts/out/DeleteDeploymentResponse';
import DeploymentRepositorySpy from '../../../repository/deploymentRepositorySpy';
import EventBusPublisherSpy from '../../../../infrastructure/message-gateway/eventBusPublisherSpy';
import { DeleteDeploymentUseCaseInterface } from '../../interfaces/in/deleteDeployment.usecase';
import DeleteDeploymentUseCase from '../DeleteDeploymentUseCase';

class SUT {
  private sut: DeleteDeploymentUseCaseInterface;

  private result: DeleteDeploymentResponse;

  private repository: DeploymentRepositorySpy;

  private eventBus: EventBusPublisherSpy;

  constructor(private testing: typeof DeleteDeploymentUseCase) {}

  public setupWith(
    repository: DeploymentRepositorySpy,
    eventBus: EventBusPublisherSpy,
    dateProvider: DateProviderInterface
  ) {
    this.repository = repository;
    this.eventBus = eventBus;
    this.sut = this.testing(repository, eventBus, dateProvider);
    return this;
  }

  public async handleCommand(command: DeleteDeploymentCommand): Promise<void> {
    this.result = await this.sut.handle(command);
  }

  public processShouldSucceed(): void {
    expect(this.result).toEqual(
      expect.objectContaining({
        id: expect.any(Number),
        // deployed_in: expect.toBeNullOr(Date),
        status: expect.any(String),
        projectId: expect.any(Number),
        createdAt: expect.any(String),
      })
    );
  }

  public shouldHaveUpdatedStatusTo(status: DeploymentStatuses): void {
    expect(this.repository.deleteSpy.status).toBe(status);
  }

  deploymentCreationMessageShouldHaveBeenPublished() {
    expect(this.eventBus.eventsSpy.length).toBeGreaterThan(0);
  }
}

let sut: SUT;
beforeAll(async () => {
  sut = new SUT(DeleteDeploymentUseCase).setupWith(
    new DeploymentRepositorySpy(),
    new EventBusPublisherSpy(),
    new DateProviderStub()
  );
});
describe('Story: User are able to soft delete a given deployment', () => {
  describe('Given incoming command to delete a given deployment', () => {
    describe('When handled by delete usecase', () => {
      it('Then it should succeed', async () => {
        const commandId = 1;

        await sut.handleCommand(commandId);

        sut.processShouldSucceed();
      });

      it(`And status should have been updated to ${DeploymentStatuses.CANCELLED}`, async () => {
        const commandId = 1;

        await sut.handleCommand(commandId);

        sut.shouldHaveUpdatedStatusTo(DeploymentStatuses.CANCELLED);
      });

      it('Then a message should have been published to external service (like data-aggregator)', async () => {
        // Arrange
        const commandId = 1;

        await sut.handleCommand(commandId);

        sut.deploymentCreationMessageShouldHaveBeenPublished();
      });
    });
  });
});

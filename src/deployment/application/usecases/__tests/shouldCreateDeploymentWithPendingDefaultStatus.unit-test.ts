import { DateProviderStub } from '../../../../__lib/core/dateProvider';
import DeploymentStatuses, {
  DEFAULT_DEPLOYMENT_STATUS,
} from '../../../../__lib/sharedKernel/deployment/DeploymentStatus';
import { CreateDeploymentCommand } from '../../../business/contracts/in/CreateDeploymentCommand';
import { CreateDeploymentResponse } from '../../../business/contracts/out/CreateDeploymentResponse';
import DeploymentRepositorySpy from '../../../repository/deploymentRepositorySpy';
import EventBusPublisherSpy from '../../../../infrastructure/message-gateway/eventBusPublisherSpy';
import { CreateDeploymentUseCaseInterface } from '../../interfaces/in/createDeployment.usecase';
import CreateDeploymentUseCase from '../CreateDeploymentUseCase';

class SUT {
  private sut: CreateDeploymentUseCaseInterface;

  private result: CreateDeploymentResponse;

  private repository: DeploymentRepositorySpy;

  private eventBus: EventBusPublisherSpy;

  constructor(private testing: typeof CreateDeploymentUseCase) {}

  setupWith(
    deploymentRepository: DeploymentRepositorySpy,
    eventBus: EventBusPublisherSpy,
    dateProvider: DateProviderStub
  ) {
    this.repository = deploymentRepository;
    this.eventBus = eventBus;
    this.sut = this.testing(deploymentRepository, eventBus, dateProvider);
    return this;
  }

  async handleCommand(command: CreateDeploymentCommand) {
    this.result = await this.sut.handle(command);
  }

  processShouldSucceed() {
    expect(this.result).toEqual(
      expect.objectContaining({
        id: expect.any(Number),
        // deployed_in: expect.any(String),
        status: DeploymentStatuses.PENDING,
        createdAt: expect.any(String),
        projectId: expect.any(Number),
      })
    );
  }

  deploymentShouldHaveBeenCreatedWith(status: DeploymentStatuses) {
    expect(this.repository.createSpy?.status).toEqual(status);
  }

  deploymentCreationMessageShouldHaveBeenPublished() {
    expect(this.eventBus.eventsSpy.length).toBeGreaterThan(0);
  }
}

let sut: SUT;
beforeEach(async () => {
  sut = new SUT(CreateDeploymentUseCase).setupWith(
    new DeploymentRepositorySpy(),
    new EventBusPublisherSpy(),
    new DateProviderStub()
  );
});
describe('Story: User are able to create a NEW Deployment for a given Project', () => {
  describe('Given incoming command to create a new deployment for a given project', () => {
    describe('When handled by create deployment usecase', () => {
      it('Then it should succeed', async () => {
        // Arrange
        const command: CreateDeploymentCommand = 1;

        // Act
        await sut.handleCommand(command);

        // Assert
        sut.processShouldSucceed();
      });

      it(`And a deployment should have been created with a default status ${DEFAULT_DEPLOYMENT_STATUS}`, async () => {
        // Arrange
        const command: CreateDeploymentCommand = 1;
        const expectedStatus = DEFAULT_DEPLOYMENT_STATUS;

        // Act
        await sut.handleCommand(command);

        // Assert
        sut.deploymentShouldHaveBeenCreatedWith(expectedStatus);
      });

      it('Then a message should have been published to external service (like data-aggregator)', async () => {
        // Arrange
        const command: CreateDeploymentCommand = 1;

        // Act
        await sut.handleCommand(command);

        // Assert
        sut.deploymentCreationMessageShouldHaveBeenPublished();
      });
    });
  });
});

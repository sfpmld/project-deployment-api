import { GetDeploymentByIdRequest } from '../../../business/contracts/in/GetDeploymentByIdRequest';
import { GetDeploymentByIdResponse } from '../../../business/contracts/out/GetDeploymentByIdResponse';
import DeploymentRepositorySpy from '../../../repository/deploymentRepositorySpy';
import { GetDeploymentByIdUseCaseInterface } from '../../interfaces/in/retrieveDeploymentById.usecase';
import GetDeploymentByIdUseCase from '../GetDeploymentByIdUseCase';

class SUT {
  private sut: GetDeploymentByIdUseCaseInterface;

  private result: GetDeploymentByIdResponse | undefined;

  constructor(private testing: typeof GetDeploymentByIdUseCase) {}

  setupWith(repository: DeploymentRepositorySpy) {
    this.sut = this.testing(repository);
    return this;
  }

  public async handleRequest(request: GetDeploymentByIdRequest) {
    this.result = await this.sut.handle(request);
  }

  public processShouldSucceed(): void {
    expect(this.result).toEqual(
      expect.objectContaining({
        id: expect.any(Number),
        deployedIn: null,
        status: expect.any(String),
        createdAt: expect.any(String),
      })
    );
  }
}

let sut: SUT;
beforeAll(async () => {
  sut = new SUT(GetDeploymentByIdUseCase).setupWith(new DeploymentRepositorySpy());
});
describe('Story: User is able to to Retrieve a given deployment based on its deployment Id ', () => {
  describe('Given incoming request to GET an existing deployment via its deployment Id', () => {
    describe('When handled by Get Deployment By Id usecase', () => {
      it('Then it should succeed', async () => {
        const request: GetDeploymentByIdRequest = 1;

        await sut.handleRequest(request);

        sut.processShouldSucceed();
      });
    });
  });
});

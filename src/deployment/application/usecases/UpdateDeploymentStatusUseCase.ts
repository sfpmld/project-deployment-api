import { DateProviderInterface } from '../../../__lib/core/dateProvider';
import DeploymentStatuses from '../../../__lib/sharedKernel/deployment/DeploymentStatus';
import calculateDatetimeDiffInSecs from '../../../__lib/utils/date';
import { UrlRandomizerInterface } from '../../../__lib/utils/urlRandomizer';
import dataMapper from '../dataMapping/index';
import {
  UpdateDeploymentStatusCommand,
  UpdateDeploymentStatusDbCommand,
} from '../../business/contracts/in';
import { UpdateDeploymentStatusResponse } from '../../business/contracts/out';
import { UpdateDeploymentStatusUseCaseInterface } from '../interfaces/in';
import { UpdateDeploymentStatusRepositoryInterface, EventBusInterface } from '../interfaces/out';
import { UpdateProjectUrlRepositoryInterface } from '../../../project/application/interfaces/out';

const isValidStatus = (status: string): boolean => {
  const validStatuses = Object.values(DeploymentStatuses);
  return validStatuses.includes(<DeploymentStatuses>status);
};

const UpdateDeploymentStatusUseCase = (
  deploymentRepository: UpdateDeploymentStatusRepositoryInterface,
  projectRepository: UpdateProjectUrlRepositoryInterface,
  eventBus: EventBusInterface,
  urlRandomizer: UrlRandomizerInterface,
  dateProvider: DateProviderInterface
): UpdateDeploymentStatusUseCaseInterface => ({
  async handle(command: UpdateDeploymentStatusCommand): Promise<UpdateDeploymentStatusResponse> {
    let updateStatusCommand: UpdateDeploymentStatusDbCommand =
      command as UpdateDeploymentStatusDbCommand;

    const validStatusCandidate = isValidStatus(command.status);
    if (!validStatusCandidate) {
      return;
    }

    const toUpdateDeployment = await deploymentRepository.getDeploymentById(command.id);
    if (!toUpdateDeployment) {
      return;
    }

    if (command.status === DeploymentStatuses.DONE) {
      const deploymentDateCreation = toUpdateDeployment.created_at;
      const deploymentDateDone = dateProvider.now();
      const deployedInTime = calculateDatetimeDiffInSecs(
        deploymentDateDone,
        deploymentDateCreation
      );
      updateStatusCommand = {
        // eslint-disable-next-line
        ...command,
        deployed_in: deployedInTime,
      };
    }
    const updatedStatusResult = await deploymentRepository.updateStatus(updateStatusCommand);
    if (updatedStatusResult) {
      const event = dataMapper.toMessage(dateProvider)({
        id: command.id,
        deployed_in: updatedStatusResult.deployed_in,
        status: command.status,
        created_at: updatedStatusResult.created_at,
        project_id: updatedStatusResult.project_id,
      });
      await eventBus.publish(event);
    }

    const isPrimerDeployment = await deploymentRepository.isPrimerDeployment(command);
    if (isPrimerDeployment && command.status === DeploymentStatuses.DONE) {
      const updateProjectUrlCommand = {
        id: command.id,
        url: urlRandomizer(command.id),
      };
      await projectRepository.updateUrl(updateProjectUrlCommand);
    }

    const result = dataMapper.toView([updatedStatusResult]);

    return result[0];
  },
});

export default UpdateDeploymentStatusUseCase;

import { DateProviderInterface } from '../../../__lib/core/dateProvider';
import { CreateDeploymentCommand } from '../../business/contracts/in/CreateDeploymentCommand';
import { CreateDeploymentResponse } from '../../business/contracts/out/CreateDeploymentResponse';
import dataMapper from '../dataMapping/index';
import { CreateDeploymentUseCaseInterface } from '../interfaces/in';
import { CreateDeploymentRepositoryInterface, EventBusInterface } from '../interfaces/out';

const CreateDeploymentUseCase = (
  repository: CreateDeploymentRepositoryInterface,
  eventBus: EventBusInterface,
  dateProvider: DateProviderInterface
): CreateDeploymentUseCaseInterface => ({
  async handle(commandId: CreateDeploymentCommand): Promise<CreateDeploymentResponse> {
    const commandToDb = dataMapper.toDb(commandId);

    const createdDeployment = await repository.create(commandToDb);

    if (createdDeployment) {
      const event = dataMapper.toMessage(dateProvider)({
        id: createdDeployment.id,
        deployed_in: createdDeployment.deployed_in,
        status: createdDeployment.status,
        created_at: createdDeployment.created_at,
        project_id: commandId,
      });
      await eventBus.publish(event);
    }

    const response = dataMapper.toView([createdDeployment]);

    return response[0];
  },
});

export default CreateDeploymentUseCase;

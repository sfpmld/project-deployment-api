import { DateProviderInterface } from '../../../__lib/core/dateProvider';
import DeploymentStatuses from '../../../__lib/sharedKernel/deployment/DeploymentStatus';
import { DeleteDeploymentCommand } from '../../business/contracts/in/DeleteDeploymentCommand';
import { DeleteDeploymentUseCaseInterface } from '../interfaces/in';
import { DeleteDeploymentRepositoryInterface, EventBusInterface } from '../interfaces/out';
import dataMapper from '../dataMapping';
import { DeleteDeploymentResponse } from '../../business/contracts/out/DeleteDeploymentResponse';

const DeleteDeploymentUseCase = (
  repository: DeleteDeploymentRepositoryInterface,
  eventBus: EventBusInterface,
  dateProvider: DateProviderInterface
): DeleteDeploymentUseCaseInterface => ({
  async handle(commandId: DeleteDeploymentCommand): Promise<DeleteDeploymentResponse> {
    const status = DeploymentStatuses.CANCELLED;

    const deleted = await repository.softDelete({ id: commandId, status });
    if (deleted) {
      const event = dataMapper.toMessage(dateProvider)({
        id: commandId,
        deployed_in: deleted.deployed_in,
        status,
        created_at: deleted.created_at,
        project_id: deleted.project_id,
      });
      await eventBus.publish(event);
    }

    const result = dataMapper.toView([deleted]);

    return result[0];
  },
});

export default DeleteDeploymentUseCase;

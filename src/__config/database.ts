export default {
  client: 'pg',
  connection: {
    port: Number.parseInt(process.env.DB_PORT || '5432', 10),
    host: process.env.POSTGRES_URL,
    user: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB,
  },
};

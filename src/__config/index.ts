import databaseConfig from './database';
import { serverConfig } from './server';

export type ParsedConfig = typeof databaseConfig & typeof serverConfig;

// eslint-disable-next-line
const config = Object.assign({}, databaseConfig, serverConfig);

export default config;

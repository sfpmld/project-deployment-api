import makeDbConnection from './infrastructure/database/sql/dbConnection/knex/pg';
import makeProjectRepository from './infrastructure/database/sql/projectRepository.adapter';
import makeDeploymentRepository from './infrastructure/database/sql/deploymentRepository.adapter';
import urlRandomizer, { UrlRandomizerInterface } from './__lib/utils/urlRandomizer';
import { dateProvider, DateProviderInterface } from './__lib/core/dateProvider';
import makeDeploymentCreationServiceForExternal from './deployment/application/services/deploymentCreationServiceForExternal';
import { ParsedConfig } from './__config';
import {
  CombinedProjectRepositoriesInterfaces,
  CombinedScopedDeploymentRepositoriesInterfaces,
} from './project/repository';
import { CombinedDeploymentRepositoriesInterfaces } from './deployment/repository';
import { CreateProjectDeploymentServiceInterface } from './project/application/interfaces/out';
import { EventBusInterface } from './deployment/application/interfaces/out';

export type AllDependencies = {
  projectRepository: CombinedProjectRepositoriesInterfaces;
  deploymentRepository: CombinedDeploymentRepositoriesInterfaces &
    CombinedScopedDeploymentRepositoriesInterfaces;
  deploymentService: CreateProjectDeploymentServiceInterface;
  eventBus: EventBusInterface;
  urlRandomizer: UrlRandomizerInterface;
  dateProvider: DateProviderInterface;
};

/** Composition root */

const bootStrapDeps = (config: ParsedConfig) => {
  const dbConnection = makeDbConnection(config);

  const eventBus = {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    async publish(param: any) {
      console.log('Event published', param);
    },
  };

  const projectRepository = makeProjectRepository(dbConnection);
  const deploymentRepository = makeDeploymentRepository(dbConnection);

  // pivot service for project service
  const deploymentCreationService = makeDeploymentCreationServiceForExternal(
    deploymentRepository,
    eventBus,
    dateProvider
  );

  const deps: AllDependencies = {
    projectRepository,
    deploymentRepository,
    deploymentService: deploymentCreationService,
    eventBus,
    urlRandomizer,
    dateProvider,
  };

  return { deps, dbConnection, config };
};

export default bootStrapDeps;

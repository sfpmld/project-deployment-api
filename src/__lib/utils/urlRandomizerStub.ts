import { UrlRandomizerInterface } from './urlRandomizer';

export const urlRandomizedDummy = 'http://thisisit.com';

const urlRandomizerStub: UrlRandomizerInterface = () => urlRandomizedDummy;
//
export default urlRandomizerStub;

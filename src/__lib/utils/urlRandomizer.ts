import { Project } from '../../project/business/entities/project';
import { DEFAULT_PROJECT_URL } from '../../__config/server';

export interface UrlRandomizerInterface {
  (id: Project['id']): string;
}

const urlRandomizer: UrlRandomizerInterface = (id: Project['id']): string => {
  const url = DEFAULT_PROJECT_URL;
  const randomizer = Math.floor(Math.random() * 1000000);
  const urlRandomized = `${url}/${id}/${randomizer}`;

  return urlRandomized;
};

export default urlRandomizer;

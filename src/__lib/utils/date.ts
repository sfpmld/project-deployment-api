const calculateDatetimeDiffInSecs = (dateDone: Date, dateCreation: Date): number => {
  const timeDiffInSecs = Math.floor(Math.abs(dateDone.getTime() - dateCreation.getTime()) / 1000);
  return timeDiffInSecs;
};

export default calculateDatetimeDiffInSecs;

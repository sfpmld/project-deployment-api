export const to = <T>(promise: Promise<T>): Promise<[Error | null, T]> =>
  promise
    .then<[null, T]>((data: T) => [null, data])
    .catch<[Error, null]>((err: Error) => [err, null]) as Promise<[Error | null, T]>;

export default to;

import { ParameterizedContext } from 'koa';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const sendOk = (ctx: ParameterizedContext) => (data: any, status?: number) => {
  ctx.status = status || 200;
  ctx.body = data;
};

export default sendOk;

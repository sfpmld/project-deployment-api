import { ParameterizedContext } from 'koa';

export const sendUnauthorized = (ctx: ParameterizedContext) => (message: string) => {
  ctx.status = 401;
  ctx.body = {
    name: 'Unauthorized',
    message: message || 'Unauthorized',
  };
};

export const sendBadRequestData = (ctx: ParameterizedContext) => (message: string) => {
  ctx.status = 400;
  ctx.body = {
    name: 'Bad Request',
    message: message || 'Bad Request',
  };
};

export const sendNotFound = (ctx: ParameterizedContext) => (message: string) => {
  ctx.status = 404;
  ctx.body = {
    name: 'Not Found',
    message: message || 'Not Found',
  };
};

export const sendServerError = (ctx: ParameterizedContext) => (message?: string) => {
  ctx.status = 500;
  ctx.body = {
    name: 'Server Error',
    message: message || 'Server Error',
  };
};

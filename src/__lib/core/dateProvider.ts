export interface DateProviderInterface {
  now(): Date;
}

export class DateProviderStub implements DateProviderInterface {
  private initDateISOString: string;

  setInitDateISOString(initDateISOString: string): void {
    this.initDateISOString = initDateISOString;
  }

  now(): Date {
    if (!this.initDateISOString) {
      console.log('DateProviderStub: initialized by default to new Date()');
      return new Date();
    }
    return new Date(this.initDateISOString);
  }
}

export const dateProvider = {
  now: () => new Date(),
};

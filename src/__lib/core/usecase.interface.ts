export interface UsecaseInterface<TReq, TRes> {
  handle(params: TReq): Promise<TRes>;
}

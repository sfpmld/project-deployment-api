import DeploymentStatuses from './DeploymentStatus';

export type DeploymentDbProps = {
  id: number;
  deployed_in: number | null;
  status: DeploymentStatuses;
  created_at: Date;
  project_id: number;
};

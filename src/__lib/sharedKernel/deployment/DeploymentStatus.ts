enum DeploymentStatuses {
  PENDING = 'pending',
  BUILDING = 'building',
  DEPLOYING = 'deploying',
  DONE = 'done',
  FAILED = 'failed',
  CANCELLED = 'cancelled',
}

export const DEFAULT_DEPLOYMENT_STATUS = DeploymentStatuses.PENDING;

export default DeploymentStatuses;

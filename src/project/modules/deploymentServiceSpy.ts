import DeploymentStatuses from '../../__lib/sharedKernel/deployment/DeploymentStatus';
import { CreateProjectDeploymentServiceInterface } from '../application/interfaces/out';
import { CreateProjectDeploymentCommand } from '../business/contracts/in';
import { CreateProjectDeploymentServiceResponse } from '../business/contracts/out';

interface DeploymentServiceSpyInterface extends CreateProjectDeploymentServiceInterface {
  createSpy: CreateProjectDeploymentCommand;
}

class DeploymentServiceSpy implements DeploymentServiceSpyInterface {
  createSpy: CreateProjectDeploymentCommand;

  async create(
    command: CreateProjectDeploymentCommand
  ): Promise<CreateProjectDeploymentServiceResponse> {
    this.createSpy = command;

    return Promise.resolve({
      id: 1,
      deployedIn: null,
      status: DeploymentStatuses.PENDING,
      createdAt: '2021-01-01T00:00:00.000Z',
      projectId: 1,
    });
  }
}

export default DeploymentServiceSpy;

import DeploymentStatuses from '../../../../__lib/sharedKernel/deployment/DeploymentStatus';
import { GetProjectByIdRequest } from '../../../business/contracts/in/GetProjectByIdRequest';
import { GetProjectByIdResponse } from '../../../business/contracts/out/GetProjectByIdResponse';
import DeploymentRepositorySpy from '../../../repository/deploymentRepositorySpy';
import ProjectRepositorySpy from '../../../repository/projectRepositorySpy';
import { RetrieveProjectByIdUseCaseInterface } from '../../interfaces/in';
import {
  GetProjectByIdRepositoryInterface,
  GetDeploymentByIdRepositoryInterface,
} from '../../interfaces/out';
import makeGetProjectByIdUseCase from '../getProjectById.usecase';

class SUT {
  private sut: RetrieveProjectByIdUseCaseInterface;

  private result: GetProjectByIdResponse;

  constructor(private testing: typeof makeGetProjectByIdUseCase) {}

  setupWith(
    projectRepository: GetProjectByIdRepositoryInterface,
    deploymentRepository: GetDeploymentByIdRepositoryInterface
  ) {
    this.sut = this.testing(projectRepository, deploymentRepository);

    return this;
  }

  async handleRequest(request: GetProjectByIdRequest) {
    this.result = await this.sut.handle(request);
  }

  processShouldSucceed() {
    expect(this.result).toBeTruthy();
  }

  liveDeploymentShouldBeProcessTo(value: boolean) {
    expect(this.result).toEqual(
      expect.objectContaining({
        hasLiveDeployment: value,
      })
    );
  }

  pendingDeploymentShouldBeProcessTo(value: boolean) {
    expect(this.result).toEqual(
      expect.objectContaining({
        hasPendingDeployment: value,
      })
    );
  }
}

let sut: SUT;
const projectRepositorySpy = new ProjectRepositorySpy();
const deploymentRepositorySpy = new DeploymentRepositorySpy();
beforeEach(async () => {
  sut = new SUT(makeGetProjectByIdUseCase).setupWith(projectRepositorySpy, deploymentRepositorySpy);
});
describe('Story: User are able to retrieve a Project by a given Id', () => {
  describe('Given incoming GET request to retrieve a given project by Id', () => {
    describe('When handled by retrieve by id usecase', () => {
      it('Then it should succeed', async () => {
        const requestId: GetProjectByIdRequest = 1;

        await sut.handleRequest(requestId);

        sut.processShouldSucceed();
      });

      const hasDeploymentCases = [
        { status: DeploymentStatuses.DONE, expected: true },
        // triangular cases
        {
          status: DeploymentStatuses.FAILED,
          expected: false,
        },
        {
          status: DeploymentStatuses.CANCELLED,
          expected: false,
        },
        {
          status: DeploymentStatuses.PENDING,
          expected: false,
        },
        {
          status: DeploymentStatuses.BUILDING,
          expected: false,
        },
        {
          status: DeploymentStatuses.DEPLOYING,
          expected: false,
        },
      ];
      describe.each(hasDeploymentCases)(
        'Then projects that have live deployment should be processed to',
        ({ status, expected }) => {
          it(`> ${expected} for status "${status}"`, async () => {
            const requestId: GetProjectByIdRequest = 1;
            deploymentRepositorySpy.setStatusTo(status);

            await sut.handleRequest(requestId);

            sut.liveDeploymentShouldBeProcessTo(expected);
          });
        }
      );

      const pendingCases = [
        { status: DeploymentStatuses.PENDING, expected: true },
        { status: DeploymentStatuses.BUILDING, expected: true },
        { status: DeploymentStatuses.DEPLOYING, expected: true },
        // triangular cases
        { status: DeploymentStatuses.FAILED, expected: false },
        { status: DeploymentStatuses.CANCELLED, expected: false },
      ];
      describe.each(pendingCases)(
        'Then projects that have pending deployments should be processed to',
        ({ status, expected }) => {
          it(`> ${expected} for status "${status}"`, async () => {
            const requestId: GetProjectByIdRequest = 1;
            deploymentRepositorySpy.setStatusTo(status);

            await sut.handleRequest(requestId);

            sut.pendingDeploymentShouldBeProcessTo(expected);
          });
        }
      );
    });
  });
});

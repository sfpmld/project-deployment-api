import DEFAULT_PAGINATION_LIMIT from '../../../../__config/pagination';
import DeploymentStatuses from '../../../../__lib/sharedKernel/deployment/DeploymentStatus';
import { ListProjectsRequest } from '../../../business/contracts/in';
import { ListProjectsResponse } from '../../../business/contracts/out';
import { ListProjectsUseCaseInterface } from '../../interfaces/in';
import {
  ListProjectsRepositoryInterface,
  GetDeploymentByIdRepositoryInterface,
} from '../../interfaces/out';
import DeploymentRepositorySpy from '../../../repository/deploymentRepositorySpy';
import ProjectRepositorySpy from '../../../repository/projectRepositorySpy';
import ListProjectsUseCase from '../listProjects.usecase';

// Test Helpers
class SUT {
  private sut: ListProjectsUseCaseInterface;

  private result: ListProjectsResponse;

  constructor(private testing: typeof ListProjectsUseCase) {}

  setupWith(
    projectRepository: ListProjectsRepositoryInterface,
    deploymentRepository: GetDeploymentByIdRepositoryInterface
  ) {
    this.sut = this.testing(projectRepository, deploymentRepository);

    return this;
  }

  async handleRequest(request: ListProjectsRequest) {
    this.result = await this.sut.handle(request);
  }

  processShouldSucceed() {
    expect(this.result.data).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          id: expect.any(Number),
          name: expect.any(String),
          // url: expect.toBeNullOrType(String),
          hasLiveDeployment: expect.any(Boolean),
          hasPendingDeployment: expect.any(Boolean),
        }),
      ])
    );
  }

  resultShouldReturnLengthOf(length: number) {
    expect(this.result.data).toHaveLength(length);
  }

  liveDeploymentShouldBeProcessTo(value: boolean) {
    expect(this.result.data).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          hasLiveDeployment: value,
        }),
      ])
    );
  }

  pendingDeploymentShouldBeProcessTo(value: boolean) {
    expect(this.result.data).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          hasPendingDeployment: value,
        }),
      ])
    );
  }
}

let sut: SUT;
const projectRepositorySpy = new ProjectRepositorySpy();
const deploymentRepositorySpy = new DeploymentRepositorySpy();
beforeEach(async () => {
  sut = new SUT(ListProjectsUseCase).setupWith(projectRepositorySpy, deploymentRepositorySpy);
});
describe('Story: User can list all existing projects', () => {
  describe('Given incoming request to list ALL existing projects', () => {
    describe('When handled by list all project usecase', () => {
      it('Then it should succeed', async () => {
        const requestDummy = { page: 1, limit: 8 };

        await sut.handleRequest(requestDummy);

        sut.processShouldSucceed();
      });

      it(`And result should have a length of ${DEFAULT_PAGINATION_LIMIT}`, async () => {
        const requestDummy = { page: 1 };

        await sut.handleRequest(requestDummy);

        sut.resultShouldReturnLengthOf(DEFAULT_PAGINATION_LIMIT);
      });

      const hasDeploymentCases = [
        { status: DeploymentStatuses.DONE, expected: true },
        // triangular cases
        {
          status: DeploymentStatuses.FAILED,
          expected: false,
        },
        {
          status: DeploymentStatuses.CANCELLED,
          expected: false,
        },
        {
          status: DeploymentStatuses.PENDING,
          expected: false,
        },
        {
          status: DeploymentStatuses.BUILDING,
          expected: false,
        },
        {
          status: DeploymentStatuses.DEPLOYING,
          expected: false,
        },
      ];
      describe.each(hasDeploymentCases)(
        'Then projects that have live deployment should be processed to',
        ({ status, expected }) => {
          it(`> ${expected} for status "${status}"`, async () => {
            const requestDummy = { page: 1 };
            deploymentRepositorySpy.setStatusTo(status);

            await sut.handleRequest(requestDummy);

            sut.liveDeploymentShouldBeProcessTo(expected);
          });
        }
      );

      const pendingCases = [
        { status: DeploymentStatuses.PENDING, expected: true },
        { status: DeploymentStatuses.BUILDING, expected: true },
        { status: DeploymentStatuses.DEPLOYING, expected: true },
        // triangular cases
        { status: DeploymentStatuses.FAILED, expected: false },
        { status: DeploymentStatuses.CANCELLED, expected: false },
      ];
      describe.each(pendingCases)(
        'Then projects that have pending deployments should be processed to',
        ({ status, expected }) => {
          it(`> ${expected} for status "${status}"`, async () => {
            const requestDummy = { page: 1 };
            deploymentRepositorySpy.setStatusTo(status);

            await sut.handleRequest(requestDummy);

            sut.pendingDeploymentShouldBeProcessTo(expected);
          });
        }
      );
    });
  });
});

import CreateProjectUseCase from '../createProjectDeployment.usecase';
import { CreateProjectDeploymentCommand } from '../../../business/contracts/in';
import { CreateProjectDeploymentUseCaseInterface } from '../../interfaces/in';
import { CreateProjectDeploymentResponse } from '../../../business/contracts/out';
import DeploymentServiceSpy from '../../../modules/deploymentServiceSpy';
import DeploymentStatuses from '../../../../__lib/sharedKernel/deployment/DeploymentStatus';

class SUT {
  private sut: CreateProjectDeploymentUseCaseInterface;

  private result: CreateProjectDeploymentResponse;

  private service: DeploymentServiceSpy;

  constructor(private testing: typeof CreateProjectUseCase) {}

  setupWith(deploymentService: DeploymentServiceSpy) {
    this.service = deploymentService;
    this.sut = this.testing(deploymentService);
    return this;
  }

  async handleCommand(command: CreateProjectDeploymentCommand) {
    this.result = await this.sut.handle(command);
  }

  processShouldSucceed() {
    expect(this.result.success).toBeTruthy();
    expect(this.result.failed).toBeFalsy();
    expect(this.result.data).toEqual({
      id: expect.any(Number),
      deployedIn: null,
      status: DeploymentStatuses.PENDING,
      createdAt: expect.any(String),
      projectId: expect.any(Number),
    });
  }

  DeploymentShouldHaveBeenCreatedWith(command: CreateProjectDeploymentCommand) {
    expect(this.service.createSpy).toEqual(command);
  }
}

let sut: SUT;
beforeAll(async () => {
  sut = new SUT(CreateProjectUseCase).setupWith(new DeploymentServiceSpy());
});
describe('Story: User are able to create a NEW Deployment for a given Project', () => {
  describe('Given incoming command to create a new deployment for a given project', () => {
    describe('When handle by create deployment usecase', () => {
      it('Then it should succeed', async () => {
        const command: CreateProjectDeploymentCommand = 2;

        await sut.handleCommand(command);

        sut.processShouldSucceed();
      });

      it('And deployment should have been created', async () => {
        const command: CreateProjectDeploymentCommand = 2;

        await sut.handleCommand(command);

        sut.DeploymentShouldHaveBeenCreatedWith(command);
      });
    });
  });
});

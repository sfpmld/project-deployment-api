import { UpdateProjectUrlCommand } from '../../../business/contracts/in';
import { UpdateProjectUrlResponse } from '../../../business/contracts/out';
import { UpdateProjectUrlUseCaseInterface } from '../../interfaces/in';
import ProjectRepositorySpy from '../../../repository/projectRepositorySpy';
import UpdateProjectUrlUseCase from '../updateProjectUrl.usecase';

class SUT {
  private sut: UpdateProjectUrlUseCaseInterface;

  private result: UpdateProjectUrlResponse;

  private projectRepository: ProjectRepositorySpy;

  constructor(private testing: typeof UpdateProjectUrlUseCase) {}

  setupWith(projectRepository: ProjectRepositorySpy) {
    this.projectRepository = projectRepository;
    this.sut = this.testing(projectRepository);
    return this;
  }

  public async handleCommand(command: UpdateProjectUrlCommand) {
    this.result = await this.sut.handle(command);
  }

  public processShouldSucceed(): void {
    expect(this.result).toBe(true);
  }

  public projectUrlShouldHaveBeenUpdatedWith(url: string): void {
    expect(this.projectRepository.updateUrlSpy).toBe(url);
  }
}

let sut: SUT;
const projectRepository = new ProjectRepositorySpy();
beforeAll(async () => {
  sut = new SUT(UpdateProjectUrlUseCase).setupWith(projectRepository);
});
describe('Story: Done 1st Deployment of a project can induce an update of project url ', () => {
  describe('Given incoming command from deployment module to update project url', () => {
    describe('When handled by update url usecase', () => {
      it('Then it should succeed', async () => {
        const command: UpdateProjectUrlCommand = {
          id: 1,
          url: 'http://www.google.com',
        };

        await sut.handleCommand(command);

        sut.processShouldSucceed();
      });
    });
  });
});

import { to } from '../../../__lib/utils/request';
import { CreateProjectDeploymentCommand } from '../../business/contracts/in';
import { CreateProjectDeploymentResponse } from '../../business/contracts/out';
import { CreateProjectDeploymentUseCaseInterface } from '../interfaces/in';
import { CreateProjectDeploymentServiceInterface } from '../interfaces/out';
import dataMapper from '../dataMapping/index';

const CreateProjectDeploymentUseCase = (
  service: CreateProjectDeploymentServiceInterface
): CreateProjectDeploymentUseCaseInterface => ({
  async handle(command: CreateProjectDeploymentCommand): Promise<CreateProjectDeploymentResponse> {
    const [error, isDeploymentCreated] = await to(service.create(command));
    if (error) {
      return dataMapper(error).toViewFailed();
    }

    return dataMapper(isDeploymentCreated).toViewSuccess();
  },
});

export default CreateProjectDeploymentUseCase;

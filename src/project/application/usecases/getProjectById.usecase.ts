import { GetProjectByIdRequest } from '../../business/contracts/in/GetProjectByIdRequest';
import { GetProjectByIdUseCaseInterface } from '../interfaces/in/getProjectById.usecase';
import {
  GetDeploymentByIdRepositoryInterface,
  GetProjectByIdRepositoryInterface,
} from '../interfaces/out';
import computeProjectStatus from '../services/computeDeploymentStatusFields';

const makeGetProjectByIdUseCase = (
  projectRepository: GetProjectByIdRepositoryInterface,
  deploymentRepository: GetDeploymentByIdRepositoryInterface
): GetProjectByIdUseCaseInterface => ({
  async handle(projectId: GetProjectByIdRequest) {
    const project = await projectRepository.getProjectById(projectId);
    if (!project) {
      return undefined;
    }

    const result = await computeProjectStatus([project], deploymentRepository);

    return result[0];
  },
});

export default makeGetProjectByIdUseCase;

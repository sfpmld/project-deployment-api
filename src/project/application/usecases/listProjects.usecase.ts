import { ListProjectsRequest } from '../../business/contracts/in';
import { ListProjectsResponse } from '../../business/contracts/out';
import { ListProjectsUseCaseInterface } from '../interfaces/in';
import {
  GetDeploymentByIdRepositoryInterface,
  ListProjectsRepositoryInterface,
} from '../interfaces/out';
import computeProjectStatus from '../services/computeDeploymentStatusFields';

const makeListProjectUseCase = (
  projectRepository: ListProjectsRepositoryInterface,
  deploymentRepository: GetDeploymentByIdRepositoryInterface
): ListProjectsUseCaseInterface => ({
  async handle(request: ListProjectsRequest): Promise<ListProjectsResponse> {
    const projects = await projectRepository.listProjects(request);
    const result = await computeProjectStatus(projects.data, deploymentRepository);

    return { data: result, total: projects.total };
  },
});

export default makeListProjectUseCase;

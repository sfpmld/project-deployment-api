import { UpdateProjectUrlCommand } from '../../business/contracts/in';
import { UpdateProjectUrlRepositoryInterface } from '../interfaces/out';

const UpdateProjectUrlUseCase = (repository: UpdateProjectUrlRepositoryInterface) => ({
  async handle(command: UpdateProjectUrlCommand) {
    const { id, url } = command;
    const response = await repository.updateUrl({ id, url });

    return response;
  },
});

export default UpdateProjectUrlUseCase;

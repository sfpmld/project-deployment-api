import { ListProjectsRequest } from '../../../business/contracts/in';
import { ListProjectsDbResponse } from '../../../business/contracts/out';

export interface ListProjectsRepositoryInterface {
  listProjects: (request: ListProjectsRequest) => Promise<ListProjectsDbResponse>;
}

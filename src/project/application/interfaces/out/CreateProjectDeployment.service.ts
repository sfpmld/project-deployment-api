import { CreateProjectDeploymentCommand } from '../../../business/contracts/in';
import { CreateProjectDeploymentServiceResponse } from '../../../business/contracts/out';

export interface CreateProjectDeploymentServiceInterface {
  create: (
    command: CreateProjectDeploymentCommand
  ) => Promise<CreateProjectDeploymentServiceResponse>;
}

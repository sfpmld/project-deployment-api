import { GetProjectByIdRequest } from '../../../business/contracts/in';
import { GetProjectByIdDbResponse } from '../../../business/contracts/out';

export interface GetProjectByIdRepositoryInterface {
  getProjectById: (request: GetProjectByIdRequest) => Promise<GetProjectByIdDbResponse>;
}

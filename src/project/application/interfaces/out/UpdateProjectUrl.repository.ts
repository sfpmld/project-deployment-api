import { UpdateProjectUrlCommand } from '../../../business/contracts/in';
import { UpdateProjectUrlResponse } from '../../../business/contracts/out';

export interface UpdateProjectUrlRepositoryInterface {
  updateUrl: (command: UpdateProjectUrlCommand) => Promise<UpdateProjectUrlResponse>;
}

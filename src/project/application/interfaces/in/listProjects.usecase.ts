import { UsecaseInterface } from '../../../../__lib/core/usecase.interface';
import { ListProjectsRequest } from '../../../business/contracts/in';
import { ListProjectsResponse } from '../../../business/contracts/out';

export interface ListProjectsUseCaseInterface
  extends UsecaseInterface<ListProjectsRequest, ListProjectsResponse> {}

export default ListProjectsUseCaseInterface;

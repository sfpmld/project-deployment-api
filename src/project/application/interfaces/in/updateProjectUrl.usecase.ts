import { UsecaseInterface } from '../../../../__lib/core/usecase.interface';
import { UpdateProjectUrlCommand } from '../../../business/contracts/in';
import { UpdateProjectUrlResponse } from '../../../business/contracts/out';

export interface UpdateProjectUrlUseCaseInterface
  extends UsecaseInterface<UpdateProjectUrlCommand, UpdateProjectUrlResponse> {}

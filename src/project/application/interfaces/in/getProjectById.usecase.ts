import { UsecaseInterface } from '../../../../__lib/core/usecase.interface';
import { GetProjectByIdRequest } from '../../../business/contracts/in/GetProjectByIdRequest';
import { GetProjectByIdResponse } from '../../../business/contracts/out/GetProjectByIdResponse';

export interface GetProjectByIdUseCaseInterface
  extends UsecaseInterface<GetProjectByIdRequest, GetProjectByIdResponse> {}

export default GetProjectByIdUseCaseInterface;

import { UsecaseInterface } from '../../../../__lib/core/usecase.interface';
import { CreateProjectDeploymentCommand } from '../../../business/contracts/in';
import { CreateProjectDeploymentResponse } from '../../../business/contracts/out';

export interface CreateProjectDeploymentUseCaseInterface
  extends UsecaseInterface<CreateProjectDeploymentCommand, CreateProjectDeploymentResponse> {}

export default CreateProjectDeploymentUseCaseInterface;

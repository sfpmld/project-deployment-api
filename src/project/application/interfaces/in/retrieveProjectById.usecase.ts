import { UsecaseInterface } from '../../../../__lib/core/usecase.interface';
import { GetProjectByIdRequest } from '../../../business/contracts/in';
import { GetProjectByIdResponse } from '../../../business/contracts/out';

export interface RetrieveProjectByIdUseCaseInterface
  extends UsecaseInterface<GetProjectByIdRequest, GetProjectByIdResponse> {}

export default RetrieveProjectByIdUseCaseInterface;

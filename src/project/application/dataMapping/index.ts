// eslint-disable-next-line @typescript-eslint/no-explicit-any
const dataMapper = <T>(data: T): any => ({
  toViewSuccess: () => ({
    success: true,
    failed: false,
    data,
  }),

  toViewFailed: (error: string | Error) => ({
    success: false,
    failed: true,
    error,
  }),
});

export default dataMapper;

import { DeploymentDbProps } from '../../../deployment/business/entities/deployment';
import DeploymentStatuses from '../../../__lib/sharedKernel/deployment/DeploymentStatus';
import { ListProjectsResponse, ListProjectsDbResponse } from '../../business/contracts/out';
import { GetDeploymentByIdRepositoryInterface } from '../interfaces/out';

const computeProjectStatus = async (
  projects: ListProjectsDbResponse['data'],
  repository: GetDeploymentByIdRepositoryInterface
): Promise<ListProjectsResponse['data']> => {
  const getDeploymentById = (id: number) => repository.getDeploymentById(id);
  const hasLiveDeploymentCondition = (deployment: DeploymentDbProps) =>
    deployment.status === DeploymentStatuses.DONE;
  const hasPendingDeploymentCondition = (deployment: DeploymentDbProps) =>
    [
      DeploymentStatuses.PENDING,
      DeploymentStatuses.BUILDING,
      DeploymentStatuses.DEPLOYING,
    ].includes(deployment.status);

  const computedProjects = projects.map(async (project) => {
    const linkedDeployment = await getDeploymentById(project.id);

    return {
      // TODO: find new way to replace spread operator
      // eslint-disable-next-line
      ...project,
      hasLiveDeployment: linkedDeployment ? hasLiveDeploymentCondition(linkedDeployment) : false,
      hasPendingDeployment: linkedDeployment
        ? hasPendingDeploymentCondition(linkedDeployment)
        : false,
    };
  });

  const result = await Promise.all(computedProjects);

  return result.map((project) => ({
    // eslint-disable-next-line
    ...project,
    url: project.url || null,
  }));
};

export default computeProjectStatus;

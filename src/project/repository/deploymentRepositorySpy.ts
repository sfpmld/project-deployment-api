import DeploymentStatuses from '../../__lib/sharedKernel/deployment/DeploymentStatus';
import { GetDeploymentByIdRequest } from '../business/contracts/in';
import { GetDeploymentByIdDbResponse } from '../business/contracts/out';
import { CombinedScopedDeploymentRepositoriesInterfaces } from './index';

class DeploymentRepositorySpy implements CombinedScopedDeploymentRepositoriesInterfaces {
  private status = DeploymentStatuses.PENDING;

  spy = [] as GetDeploymentByIdDbResponse[];

  setStatusTo(status: DeploymentStatuses): void {
    this.status = status;
  }

  async getDeploymentById(request: GetDeploymentByIdRequest): Promise<GetDeploymentByIdDbResponse> {
    console.log('retrieving deployment by id', request);
    return {
      id: 1,
      status: this.status,
      deployed_in: 3,
      created_at: new Date('2021-01-01'),
      project_id: 1,
    };

    // return deploymentDummies.slice(0, limit);
  }
}

export default DeploymentRepositorySpy;

import {
  CreateProjectDeploymentCommand,
  ListProjectsRequest,
  GetProjectByIdRequest,
  UpdateProjectUrlCommand,
} from '../business/contracts/in';
import {
  GetProjectByIdDbResponse,
  UpdateProjectUrlResponse,
  ListProjectsDbResponse,
} from '../business/contracts/out';
import { LookupDeploymentAppSecretResponse } from '../../deployment/business/contracts/out';
import { LookupDeploymentAppSecretRequest } from '../../deployment/business/contracts/in';
import projectDummies from '../../../testSettings/fixtures/projectsComputed.json';
import paginationService from '../../infrastructure/database/sql/pagination';
import { ProjectDbProps } from '../business/entities/project';
import { CombinedProjectRepositoriesInterfaces } from './index';

class ProjectRepositorySpy implements CombinedProjectRepositoriesInterfaces {
  createSpy = {} as CreateProjectDeploymentCommand;

  updateUrlSpy = {} as UpdateProjectUrlCommand;

  async listProjects(request: ListProjectsRequest): Promise<ListProjectsDbResponse> {
    const { limit } = paginationService(request.page, request.limit);
    return { data: projectDummies.slice(0, limit), total: projectDummies.length };
  }

  async getProjectById(request: GetProjectByIdRequest): Promise<GetProjectByIdDbResponse> {
    return projectDummies.find((project) => project.id === request);
  }

  async updateUrl(command: UpdateProjectUrlCommand): Promise<UpdateProjectUrlResponse> {
    const toUpdateProject = projectDummies.find(
      (project) => project.id === command.id
    ) as ProjectDbProps;
    if (toUpdateProject) {
      toUpdateProject.url = command.url;
      this.updateUrlSpy.url = toUpdateProject.url;
      return Promise.resolve(true);
    }

    return Promise.resolve(false);
  }

  async lookupAppSecret(
    request: LookupDeploymentAppSecretRequest
  ): Promise<LookupDeploymentAppSecretResponse> {
    const isAuthorized = projectDummies.find(
      (project) =>
        project.id === request.id && project.app_secret === request.bearerAgainstAppSecret
    );

    return Promise.resolve(!!isAuthorized);
  }
}

export default ProjectRepositorySpy;

import { LookupDeploymentAppSecretRepositoryInterface } from '../../deployment/application/interfaces/out';
import {
  GetDeploymentByIdRepositoryInterface,
  GetProjectByIdRepositoryInterface,
  ListProjectsRepositoryInterface,
  UpdateProjectUrlRepositoryInterface,
} from '../application/interfaces/out';

export interface CombinedProjectRepositoriesInterfaces
  extends ListProjectsRepositoryInterface,
    GetProjectByIdRepositoryInterface,
    UpdateProjectUrlRepositoryInterface,
    LookupDeploymentAppSecretRepositoryInterface {}

export interface CombinedScopedDeploymentRepositoriesInterfaces
  extends GetDeploymentByIdRepositoryInterface {}

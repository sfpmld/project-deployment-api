export type UpdateProjectUrlCommand = {
  id: number;
  url: string;
};

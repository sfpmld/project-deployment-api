import { Deployment } from '../../entities/deployment';

export type GetDeploymentByIdRequest = Deployment['id'];

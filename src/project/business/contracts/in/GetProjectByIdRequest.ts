import { Project } from '../../entities/project';

export type GetProjectByIdRequest = Project['id'];

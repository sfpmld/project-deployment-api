export type ListProjectsRequest = {
  page: number;
  limit?: number;
};

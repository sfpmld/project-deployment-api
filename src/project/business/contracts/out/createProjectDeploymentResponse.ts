import { Deployment } from '../../entities/deployment';

export type CreateProjectDeploymentServiceResponse = Deployment;

export type CreateProjectDeploymentResponse = {
  success: boolean;
  failed: boolean;
  data?: CreateProjectDeploymentServiceResponse;
  error?: string | Error;
};

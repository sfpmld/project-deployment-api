import { Project } from '../../entities/project';

export type GetProjectByIdResponse = Project | undefined;

import { ProjectDbProps } from '../../entities/project';

export type GetProjectByIdDbResponse = ProjectDbProps | undefined;

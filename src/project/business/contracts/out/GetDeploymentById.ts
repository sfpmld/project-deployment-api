import { DeploymentDbProps } from '../../../../__lib/sharedKernel/deployment/Deployment';

export type GetDeploymentByIdDbResponse = DeploymentDbProps;

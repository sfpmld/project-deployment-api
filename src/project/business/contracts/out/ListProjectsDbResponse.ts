import { ProjectDbProps } from '../../entities/project';

export type ListProjectsDbResponse = { data: ProjectDbProps[]; total: number };

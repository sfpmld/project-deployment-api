import { Project } from '../../entities/project';

export type ListProjectsResponse = { data: Project[]; total: number };

export type ProjectDbProps = {
  id: number;
  name: string;
  url?: string | null;
  app_secret: string;
  created_at: string;
};

export type Project = {
  id: number;
  name: string;
  hasLiveDeployment: boolean;
  hasPendingDeployment: boolean;
  url?: string | null;
};

import DeploymentStatuses from '../../../__lib/sharedKernel/deployment/DeploymentStatus';

export type Deployment = {
  id: number;
  deployedIn: number | null;
  status: DeploymentStatuses;
  createdAt: string;
  projectId: number;
};

import config from './__config';
import { startHttpServer } from './infrastructure/http-gateway/rest/server';
import makeInitializedDependencies from './bootstrapDeps';
import makeInitializedUseCases from './bootstrap';

/** Composition root */

const dependencies = makeInitializedDependencies(config);
const useCases = makeInitializedUseCases(dependencies.deps);

// start transport
startHttpServer(dependencies.deps, useCases, config);

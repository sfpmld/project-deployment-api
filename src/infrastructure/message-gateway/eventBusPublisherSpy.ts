import { EventBusInterface } from '../../deployment/application/interfaces/out';
import { DeploymentStatusChangedMessage } from '../../deployment/business/contracts/out';

class EventBusPublisherSpy implements EventBusInterface {
  eventsSpy: DeploymentStatusChangedMessage[] = [];

  async publish(event: DeploymentStatusChangedMessage): Promise<void> {
    this.eventsSpy.push(event);
  }
}

export default EventBusPublisherSpy;

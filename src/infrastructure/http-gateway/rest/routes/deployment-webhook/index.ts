import Router from '@koa/router';
import { CombinedDeploymentRepositoriesInterfaces } from '../../../../../deployment/repository';
import {
  CombinedProjectRepositoriesInterfaces,
  CombinedScopedDeploymentRepositoriesInterfaces,
} from '../../../../../project/repository';
import makeWebhookDeploymentUpdateController from '../../controllers/webhook-deployment/makeWebhookDeploymentUpdateController';
import makeAuthMiddleware from '../../middlewares/authorization';
import { UpdateDeploymentStatusUseCaseInterface } from '../../../../../deployment/application/interfaces/in/updateDeploymentStatus.usecase';

const makeWebhookRoutes =
  (
    deploymentRepository: CombinedDeploymentRepositoriesInterfaces &
      CombinedScopedDeploymentRepositoriesInterfaces,
    projectRepository: CombinedProjectRepositoriesInterfaces,
    updateDeploymentusecase: UpdateDeploymentStatusUseCaseInterface
  ) =>
  (router: Router) => {
    router.post(
      '/deployments/webhook',
      makeAuthMiddleware(deploymentRepository, projectRepository),
      makeWebhookDeploymentUpdateController(updateDeploymentusecase)
    );
    return router;
  };

export default makeWebhookRoutes;

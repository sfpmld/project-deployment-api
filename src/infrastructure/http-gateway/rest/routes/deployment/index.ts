import Router from '@koa/router';
import paginationMiddleware from '../../middlewares/pagination';
import makeListDeploymentsController from '../../controllers/deployments/listDeploymentsController';
import makeGetDeploymentByIdController from '../../controllers/deployments/makeGetDeploymentByIdController';
import makeDeleteDeploymentByIdController from '../../controllers/deployments/makeDeleteDeploymentController';
import { GetDeploymentsUseCaseInterface } from '../../../../../deployment/application/interfaces/in/retrieveDeployments.usecase';
import { GetDeploymentByIdUseCaseInterface } from '../../../../../deployment/application/interfaces/in/retrieveDeploymentById.usecase';
import { DeleteDeploymentUseCaseInterface } from '../../../../../deployment/application/interfaces/in/deleteDeployment.usecase';

const makeDeploymentRoutes =
  (
    listDeploymentsUseCase: GetDeploymentsUseCaseInterface,
    getDeploymentByIdUseCase: GetDeploymentByIdUseCaseInterface,
    deleteDeploymentByIdUseCase: DeleteDeploymentUseCaseInterface
  ) =>
  (router: Router) => {
    router
      .get(
        '/deployments',
        makeListDeploymentsController(listDeploymentsUseCase),
        paginationMiddleware
      )
      .get('/deployments/:id', makeGetDeploymentByIdController(getDeploymentByIdUseCase))
      .post(
        '/deployments/:id/cancel',
        makeDeleteDeploymentByIdController(deleteDeploymentByIdUseCase)
      );
    return router;
  };

export default makeDeploymentRoutes;

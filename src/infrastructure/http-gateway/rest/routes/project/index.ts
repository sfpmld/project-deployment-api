import Router from '@koa/router';
import { CreateProjectDeploymentServiceInterface } from '../../../../../project/application/interfaces/out';
import paginationMiddleware from '../../middlewares/pagination';
import makeListProjectController from '../../controllers/project/ListProjectsController';
import makeGetProjectByIdController from '../../controllers/project/getProjectByIdController';
import createProjectDeploymentController from '../../controllers/project/createProjectDeploymentController';
import { ListProjectsUseCaseInterface } from '../../../../../project/application/interfaces/in/listProjects.usecase';
import { GetProjectByIdUseCaseInterface } from '../../../../../project/application/interfaces/in/getProjectById.usecase';

const makeProjectRoutes =
  (
    listProjectsUseCase: ListProjectsUseCaseInterface,
    getProjectByIdUseCase: GetProjectByIdUseCaseInterface,
    createProjectDeploymentService: CreateProjectDeploymentServiceInterface
  ) =>
  (router: Router) => {
    router
      .get('/projects', makeListProjectController(listProjectsUseCase), paginationMiddleware)
      .get('/projects/:id', makeGetProjectByIdController(getProjectByIdUseCase))
      .post(
        '/projects/:id/deployments',
        createProjectDeploymentController(createProjectDeploymentService)
      );

    return router;
  };

export default makeProjectRoutes;

import convert from 'koa-convert';
import cors from 'koa-cors';
import bodyParser from 'koa-bodyparser';

const makeCommonMiddlewares = () => {
  return [convert(cors()), bodyParser()];
};

export default makeCommonMiddlewares;

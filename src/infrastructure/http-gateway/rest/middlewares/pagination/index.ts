import * as Koa from 'koa';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const paginationMiddleware = async (ctx: Koa.Context, next: Koa.Next) => {
  await next();

  const { page, totalPages } = ctx.state;
  const baseUrl = `http://${ctx.host}${ctx.path}`;

  const links: string[] = [];
  links.push(`<${baseUrl}?page=1>; rel="first"`);
  if (page > 1) {
    links.push(`<${baseUrl}?page=${page - 1}>; rel="prev"`);
  }
  if (page < totalPages) {
    links.push(`<${baseUrl}?page=${page + 1}>; rel="next"`);
  }

  links.push(`<${baseUrl}?page=${totalPages}>; rel="last"`);

  ctx.set('Pagination', links.join(', '));
};

export default paginationMiddleware;

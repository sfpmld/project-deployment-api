import { Next, ParameterizedContext } from 'koa';
import { CombinedDeploymentRepositoriesInterfaces } from '../../../../../deployment/repository';
import {
  CombinedProjectRepositoriesInterfaces,
  CombinedScopedDeploymentRepositoriesInterfaces,
} from '../../../../../project/repository';
import { sendNotFound, sendUnauthorized } from '../../exceptions';

const authMiddleware =
  (
    deploymentRepository: CombinedDeploymentRepositoriesInterfaces &
      CombinedScopedDeploymentRepositoriesInterfaces,
    projectRepository: CombinedProjectRepositoriesInterfaces
  ) =>
  async (ctx: ParameterizedContext, next: Next) => {
    const { authorization } = ctx.headers;
    if (!authorization) {
      sendUnauthorized(ctx)('Missing authorization header');
      return;
    }

    const [type, token] = authorization.split(' ');
    if (type !== 'Bearer') {
      ctx.status = 401;
      sendUnauthorized(ctx)("Missing 'Bearer' in authorization header");
      return;
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const payload = ctx.request.body as any;
    const { id: deploymentId } = payload;

    const project = await deploymentRepository.getDeploymentById(deploymentId);
    if (!project) {
      sendNotFound(ctx)('Deployment not found');
      return;
    }

    const request = {
      id: project.project_id,
      bearerAgainstAppSecret: token,
    };
    const isAuthorized = await projectRepository.lookupAppSecret(request);
    if (!isAuthorized) {
      sendUnauthorized(ctx)('Not the same user, or bad token');
      return;
    }

    await next();
  };

export default authMiddleware;

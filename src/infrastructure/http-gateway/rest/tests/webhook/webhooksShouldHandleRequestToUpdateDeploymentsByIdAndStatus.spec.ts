import request from 'supertest';
import { startHttpServer, stopHttpServer } from '../../server';
import config from '../../../../../__config';
import initDependencies from '../../../../../bootstrapDeps';
import initUseCases from '../../../../../bootstrap';
import { seedDeployments, truncateData } from '../../../../../../testSettings/testHelpers/dbUtils';
import DeploymentStatuses from '../../../../../__lib/sharedKernel/deployment/DeploymentStatus';
import deploymentDummy from '../../../../../../testSettings/fixtures/deployments.json';
import projectDummy from '../../../../../../testSettings/fixtures/projects.json';

const bootstraped = initDependencies(config);
const useCases = initUseCases(bootstraped.deps);

let app: unknown;
beforeAll(async () => {
  app = await startHttpServer(bootstraped.deps, useCases, config);
});
beforeEach(async () => {
  await seedDeployments(bootstraped.dbConnection);
});
afterEach(async () => {
  await truncateData(bootstraped.dbConnection);
});
afterAll(async () => {
  await stopHttpServer();
});
describe('Webhook >', () => {
  describe('Scenario: Authenticated user', () => {
    describe("POST '/deployments/webhook' update a given deployment's 'status' based on its given 'id'", () => {
      describe('Given incoming http request for webhook to update an existing deployment based on its Id', () => {
        describe('When received by the service', () => {
          it('Then it should succeed', async () => {
            // Arrange
            const command = {
              id: deploymentDummy[2].id,
              status: DeploymentStatuses.FAILED,
            };
            const appSecret = projectDummy[0].app_secret;
            const bearerHeader = `Bearer ${appSecret}`;

            // Act
            const result = await request(app)
              .post('/deployments/webhook')
              .send(command)
              .set('Authorization', bearerHeader);

            // Assert
            expect(result.status).toEqual(200);
            expect(result.body.data).toEqual(
              expect.objectContaining({
                id: expect.any(Number),
                deployedIn: null,
                createdAt: expect.any(String),
                projectId: expect.any(Number),
              })
            );
          });
        });
      });
    });
  });

  describe('Scenario: UNAuthenticated user', () => {
    describe("POST '/deployments/webhook' update a given deployment's 'status' based on its given 'id'", () => {
      describe('Given incoming http request for webhook to update an existing deployment based on its Id', () => {
        describe('When received by the service', () => {
          it('Then it should send 401 status UnAuthorized', async () => {
            // Arrange
            const command = {
              id: deploymentDummy[2].id,
              status: DeploymentStatuses.FAILED,
            };
            const appSecret = projectDummy[5].app_secret;
            const bearerHeader = `Bearer ${appSecret}`;

            // Act
            const result = await request(app)
              .post('/deployments/webhook')
              .send(command)
              .set('Authorization', bearerHeader);

            // Assert
            expect(result.status).toEqual(401);
            expect(result.body.name).toMatch(/Unauthorized/);
          });
        });
      });
    });
  });
});

import request from 'supertest';
import { startHttpServer, stopHttpServer } from '../../server';
import config from '../../../../../__config';
import initDependencies from '../../../../../bootstrapDeps';
import initUseCases from '../../../../../bootstrap';
import { seedDeployments, truncateData } from '../../../../../../testSettings/testHelpers/dbUtils';

const bootstraped = initDependencies(config);
const useCases = initUseCases(bootstraped.deps);

let app: unknown;
beforeAll(async () => {
  app = await startHttpServer(bootstraped.deps, useCases, config);
});
beforeEach(async () => {
  await seedDeployments(bootstraped.dbConnection);
});
afterEach(async () => {
  await truncateData(bootstraped.dbConnection);
});
afterAll(async () => {
  await stopHttpServer();
});
describe('GET "/projects/{id}": returns a given project by its Id', () => {
  describe('Given incoming http request to retrieve a given project by its Id', () => {
    describe('When received by the service', () => {
      it('Then it should succeed', async () => {
        // Arrange
        const requestId = 2;

        // Act
        const result = await request(app).get(`/projects/${requestId}`);

        // Assert
        expect(result.status).toBe(200);
        expect(result.body.data).toEqual(
          expect.objectContaining({
            id: expect.any(Number),
            name: expect.any(String),
            // url: expect.toBeNullOrType(String),
            hasLiveDeployment: expect.any(Boolean),
            hasPendingDeployment: expect.any(Boolean),
          })
        );
      });
    });
  });
});

import request from 'supertest';
import { startHttpServer, stopHttpServer } from '../../server';
import config from '../../../../../__config';
import initDependencies from '../../../../../bootstrapDeps';
import initUseCases from '../../../../../bootstrap';
import { seedDeployments, truncateData } from '../../../../../../testSettings/testHelpers/dbUtils';

const bootstraped = initDependencies(config);
const useCases = initUseCases(bootstraped.deps);

let app: unknown;
beforeAll(async () => {
  await truncateData(bootstraped.dbConnection);
  app = await startHttpServer(bootstraped.deps, useCases, config);
});
beforeEach(async () => {
  await seedDeployments(bootstraped.dbConnection);
});
afterEach(async () => {
  await truncateData(bootstraped.dbConnection);
});
afterAll(async () => {
  await stopHttpServer();
});

describe('POST "/projects/:id/deployments" trigger a new deployment for a given project', () => {
  describe('Given incoming http request to list all existing project', () => {
    describe('When received by the service', () => {
      it('Then it should succeed', async () => {
        // Arrange
        const commandId = 1;

        // Act
        const result = await request(app).post(`/projects/${commandId}/deployments`);

        // Assert
        expect(result.status).toBe(201);
        expect(result.body.data).toEqual(
          expect.objectContaining({
            id: expect.any(Number),
            deployedIn: null,
            createdAt: expect.any(String),
            projectId: commandId,
          })
        );
      });
    });
  });
});

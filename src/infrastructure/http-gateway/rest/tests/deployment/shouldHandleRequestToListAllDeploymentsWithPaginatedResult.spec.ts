import request from 'supertest';
import { startHttpServer, stopHttpServer } from '../../server';
import config from '../../../../../__config';
import initDependencies from '../../../../../bootstrapDeps';
import initUseCases from '../../../../../bootstrap';
import { seedDeployments, truncateData } from '../../../../../../testSettings/testHelpers/dbUtils';

const bootstraped = initDependencies(config);
const useCases = initUseCases(bootstraped.deps);

let app: unknown;
beforeAll(async () => {
  app = await startHttpServer(bootstraped.deps, useCases, config);
});
beforeEach(async () => {
  await seedDeployments(bootstraped.dbConnection);
});
afterEach(async () => {
  await truncateData(bootstraped.dbConnection);
});
afterAll(async () => {
  await stopHttpServer();
});
describe('GET "/deployments" return the list of existing deployment with paginated results', () => {
  describe('Given incoming http request to list all existing deployment', () => {
    describe('When received by the service', () => {
      it('Then it should succeed', async () => {
        // Arrange
        const page = 1;

        // Act
        const result = await request(app).get(`/deployments?page=${page}`);

        // Assert
        expect(result.status).toEqual(200);
        expect(result.headers.pagination).toBe(
          '<http://127.0.0.1:3000/deployments?page=1>; rel="first", <http://127.0.0.1:3000/deployments?page=2>; rel="next", <http://127.0.0.1:3000/deployments?page=10>; rel="last"'
        );
        expect(result.body.data).toEqual(
          expect.arrayContaining([
            expect.objectContaining({
              id: expect.any(Number),
              deployedIn: null,
              createdAt: expect.any(String),
              projectId: expect.any(Number),
            }),
          ])
        );
      });
    });
  });
});

import request from 'supertest';
import { startHttpServer, stopHttpServer } from '../../server';
import config from '../../../../../__config';
import initDependencies from '../../../../../bootstrapDeps';
import initUseCases from '../../../../../bootstrap';
import { seedDeployments, truncateData } from '../../../../../../testSettings/testHelpers/dbUtils';

const bootstraped = initDependencies(config);
const useCases = initUseCases(bootstraped.deps);

let app: unknown;
beforeAll(async () => {
  app = await startHttpServer(bootstraped.deps, useCases, config);
});
beforeEach(async () => {
  await seedDeployments(bootstraped.dbConnection);
});
afterEach(async () => {
  await truncateData(bootstraped.dbConnection);
});
afterAll(async () => {
  await stopHttpServer();
});
describe('POST "/deployments/{id}/cancel" delete a given deployment based on its Id', () => {
  describe('Given incoming http request to delete an existing deployment based on its Id', () => {
    describe('When received by the service', () => {
      it('Then it should succeed', async () => {
        // Arrange
        const deploymentId = 3;

        // Act
        const result = await request(app).post(`/deployments/${deploymentId}/cancel`);

        // Assert
        expect(result.status).toEqual(200);
        expect(result.body.data).toEqual(
          expect.objectContaining({
            id: expect.any(Number),
            deployedIn: null,
            createdAt: expect.any(String),
            projectId: expect.any(Number),
          })
        );
      });
    });
  });
});

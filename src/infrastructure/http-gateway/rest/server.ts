import Koa, { DefaultContext, DefaultState, Middleware } from 'koa';
import KoaRouter from '@koa/router';
import R from 'ramda';
import { ParsedConfig } from '../../../__config';
import makeDeploymentRoutes from './routes/deployment';
import makeDeploymentWebhookRoutes from './routes/deployment-webhook';
import { CombinedDeploymentRepositoriesInterfaces } from '../../../deployment/repository';
import makeCommonMiddlewares from './middlewares/common';
import {
  CombinedProjectRepositoriesInterfaces,
  CombinedScopedDeploymentRepositoriesInterfaces,
} from '../../../project/repository';
import makeProjectRoutes from './routes/project';
import { CreateProjectDeploymentServiceInterface } from '../../../project/application/interfaces/out';
import { CreateDeploymentUseCaseInterface } from '../../../deployment/application/interfaces/in/createDeployment.usecase';
import { DeleteDeploymentUseCaseInterface } from '../../../deployment/application/interfaces/in/deleteDeployment.usecase';
import { GetDeploymentByIdUseCaseInterface } from '../../../deployment/application/interfaces/in/retrieveDeploymentById.usecase';
import { GetDeploymentsUseCaseInterface } from '../../../deployment/application/interfaces/in/retrieveDeployments.usecase';
import { UpdateDeploymentStatusUseCaseInterface } from '../../../deployment/application/interfaces/in/updateDeploymentStatus.usecase';
import { CreateProjectDeploymentUseCaseInterface } from '../../../project/application/interfaces/in/createProjectDeployment.usecase';
import { GetProjectByIdUseCaseInterface } from '../../../project/application/interfaces/in/getProjectById.usecase';
import { ListProjectsUseCaseInterface } from '../../../project/application/interfaces/in/listProjects.usecase';
import { UpdateProjectUrlUseCaseInterface } from '../../../project/application/interfaces/in/updateProjectUrl.usecase';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let httpServerClosure: any;

const applyCommonMiddlewares = (
  appServer: Koa,
  middlewaresArray: Array<Middleware<DefaultState, DefaultContext>>
): void => {
  middlewaresArray.forEach((middleware) => {
    appServer.use(middleware);
  });
};

export type AllUseCasesInterface = {
  // deployment
  createDeploymentUseCase: CreateDeploymentUseCaseInterface;
  deleteDeploymentUseCase: DeleteDeploymentUseCaseInterface;
  getDeploymentByIdUseCase: GetDeploymentByIdUseCaseInterface;
  getDeploymentsUseCase: GetDeploymentsUseCaseInterface;
  updateDeploymentStatusUseCase: UpdateDeploymentStatusUseCaseInterface;
  // project
  createProjectDeploymentUseCase: CreateProjectDeploymentUseCaseInterface;
  getProjectByIdUseCase: GetProjectByIdUseCaseInterface;
  listProjectsUseCase: ListProjectsUseCaseInterface;
  updateProjectUrlUseCase: UpdateProjectUrlUseCaseInterface;
};

export type NeededDependenciesInterface = {
  projectRepository: CombinedProjectRepositoriesInterfaces;
  deploymentRepository: CombinedDeploymentRepositoriesInterfaces &
    CombinedScopedDeploymentRepositoriesInterfaces;
  deploymentService: CreateProjectDeploymentServiceInterface;
};

const appFactory = (deps: NeededDependenciesInterface, usecases: AllUseCasesInterface) => {
  const { deploymentService, projectRepository, deploymentRepository } = deps;
  const {
    deleteDeploymentUseCase,
    getDeploymentByIdUseCase,
    getDeploymentsUseCase,
    updateDeploymentStatusUseCase,
    getProjectByIdUseCase,
    listProjectsUseCase,
  } = usecases;

  // init
  const app = new Koa();
  const router = new KoaRouter();

  // Init middlewares (app, [...]);
  const commonMiddlewares = makeCommonMiddlewares();
  applyCommonMiddlewares(app, commonMiddlewares);

  // Init routes
  const deploymentRoutes = makeDeploymentRoutes(
    getDeploymentsUseCase,
    getDeploymentByIdUseCase,
    deleteDeploymentUseCase
  );
  const projectRoutes = makeProjectRoutes(
    listProjectsUseCase,
    getProjectByIdUseCase,
    deploymentService
  );
  const deploymentWebhookRoutes = makeDeploymentWebhookRoutes(
    deploymentRepository,
    projectRepository,
    updateDeploymentStatusUseCase
  );

  const Router = R.pipe(deploymentRoutes, projectRoutes, deploymentWebhookRoutes)(router);

  // Apply routes
  app.use(Router.routes());

  // Apply allowed methods
  app.use(
    Router.allowedMethods({
      throw: true,
    })
  );

  return app;
};

// PUBLIC API
export const startHttpServer = (
  dependencies: NeededDependenciesInterface,
  usecases: AllUseCasesInterface,
  config: ParsedConfig
) => {
  const { port } = config.server;
  const app = appFactory(dependencies, usecases);

  const httpServer = new Promise((resolve, reject) => {
    try {
      resolve(
        app.listen(port, () => {
          resolve(this);
        })
      );
    } catch (err) {
      reject(err);
    }
  });

  return httpServer
    .then(async (server) => {
      console.info(`Server listening at ${port}`);
      httpServerClosure = server;
      return server;
    })
    .catch((err) => console.error(`Server start failed: ${err}`));
};

export const stopHttpServer = async () => {
  if (httpServerClosure) {
    await httpServerClosure.close();
  }
};

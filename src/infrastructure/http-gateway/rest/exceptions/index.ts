import { ParameterizedContext } from 'koa';

export const sendUnauthorized = (ctx: ParameterizedContext) => (message: string) => {
  ctx.status = 401;
  ctx.body = {
    name: 'Unauthorized',
    message: message || 'Unauthorized',
  };
};

export const sendNotFound = (ctx: ParameterizedContext) => (message: string) => {
  ctx.status = 404;
  ctx.body = {
    name: 'Not Found',
    message: message || 'Not Found',
  };
};

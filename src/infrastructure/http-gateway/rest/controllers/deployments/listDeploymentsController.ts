import { Next, ParameterizedContext } from 'koa';
import sendOk from '../../../../../__lib/core/response/ok';
import { GetDeploymentsUseCaseInterface } from '../../../../../deployment/application/interfaces/in/retrieveDeployments.usecase';

const makeListDeploymentsController =
  (usecase: GetDeploymentsUseCaseInterface) => async (ctx: ParameterizedContext, next: Next) => {
    const { page } = ctx.request.query;
    if (!page) throw new Error('page is required');

    const request = {
      page: parseInt(<string>page, 10) || 1,
    };
    const result = await usecase.handle(request);

    ctx.state = {
      page: request.page,
      totalPages: result.total,
    };
    sendOk(ctx)(result);

    await next();
  };

export default makeListDeploymentsController;

import { Next, ParameterizedContext } from 'koa';
import sendOk from '../../../../../__lib/core/response/ok';
import { sendBadRequestData } from '../../../../../__lib/core/response/error';
import { GetDeploymentByIdUseCaseInterface } from '../../../../../deployment/application/interfaces/in/retrieveDeploymentById.usecase';

const makeGetDeploymentByIdController =
  (usecase: GetDeploymentByIdUseCaseInterface) => async (ctx: ParameterizedContext, next: Next) => {
    const { id } = ctx.params;
    if (!id) sendBadRequestData(ctx)('id is required');

    const requestId = parseInt(<string>id, 10);
    const result = await usecase.handle(requestId);

    sendOk(ctx)({ data: result });

    await next();
  };

export default makeGetDeploymentByIdController;

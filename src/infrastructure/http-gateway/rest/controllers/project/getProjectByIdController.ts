import { Next, ParameterizedContext } from 'koa';
import { GetProjectByIdUseCaseInterface } from '../../../../../project/application/interfaces/in/getProjectById.usecase';
import { sendBadRequestData } from '../../../../../__lib/core/response/error';
import sendOk from '../../../../../__lib/core/response/ok';

const makeGetProjectByIdController =
  (usecase: GetProjectByIdUseCaseInterface) => async (ctx: ParameterizedContext, next: Next) => {
    const { id } = ctx.params;
    if (!id) sendBadRequestData(ctx)('id is required');

    const requestId = parseInt(<string>id, 10);
    const result = await usecase.handle(requestId);

    sendOk(ctx)({ data: result });

    await next();
  };

export default makeGetProjectByIdController;

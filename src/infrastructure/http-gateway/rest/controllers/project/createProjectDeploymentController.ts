import { Next, ParameterizedContext } from 'koa';
import { CreateProjectDeploymentServiceInterface } from '../../../../../project/application/interfaces/out/CreateProjectDeployment.service';
import makeCreateProjectDeploymentUseCase from '../../../../../project/application/usecases/createProjectDeployment.usecase';
import sendOk from '../../../../../__lib/core/response/ok';

const makeCreateProjectDeploymentController =
  (deploymentService: CreateProjectDeploymentServiceInterface) =>
  async (ctx: ParameterizedContext, next: Next) => {
    const { id } = ctx.params;
    if (!id) throw new Error('id is required');

    const usecase = await makeCreateProjectDeploymentUseCase(deploymentService);
    const commandId = parseInt(<string>id, 10);
    const result = await usecase.handle(commandId);

    if (result.success) {
      sendOk(ctx)({ data: result.data }, 201);
    } else {
      ctx.body = {
        message: result.error,
      };
      ctx.status = 500;
    }

    await next();
  };

export default makeCreateProjectDeploymentController;

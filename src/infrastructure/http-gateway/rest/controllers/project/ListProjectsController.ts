import { Next, ParameterizedContext } from 'koa';
import { ListProjectsUseCaseInterface } from '../../../../../project/application/interfaces/in/listProjects.usecase';
import { sendServerError } from '../../../../../__lib/core/response/error';
import sendOk from '../../../../../__lib/core/response/ok';
import { to } from '../../../../../__lib/utils/request';

const listProjectController =
  (usecase: ListProjectsUseCaseInterface) => async (ctx: ParameterizedContext, next: Next) => {
    const { page } = ctx.request.query;
    if (!page) throw new Error('page is required');

    const request = {
      page: parseInt(<string>page, 10) || 1,
    };

    const [error, result] = await to(usecase.handle(request));
    if (error) {
      sendServerError(ctx)(error.message);
      await next();
      return;
    }

    ctx.state = {
      page: request.page,
      totalPages: result.total,
    };

    sendOk(ctx)(result);

    await next();
  };

export default listProjectController;

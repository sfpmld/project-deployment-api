import { Next, ParameterizedContext } from 'koa';
import { UpdateDeploymentStatusCommand } from '../../../../../deployment/business/contracts/in';
import sendOk from '../../../../../__lib/core/response/ok';
import { UpdateDeploymentStatusUseCaseInterface } from '../../../../../deployment/application/interfaces/in/updateDeploymentStatus.usecase';

const makeWebhookDeploymentUpdateController =
  (usecase: UpdateDeploymentStatusUseCaseInterface) =>
  async (ctx: ParameterizedContext, next: Next) => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const payload = ctx.request.body as any;
    const { id, status } = payload;
    const command: UpdateDeploymentStatusCommand = {
      id,
      status,
    };
    const response = await usecase.handle(command);

    sendOk(ctx)({ data: response });
    await next();
  };

export default makeWebhookDeploymentUpdateController;

import { Knex } from 'knex';
import {
  CreateDeploymentDbCommand,
  DeleteDeploymentDbCommand,
  GetDeploymentByIdRequest,
  GetDeploymentsRequest,
  UpdateDeploymentStatusCommand,
  UpdateDeploymentStatusDbCommand,
} from '../../../deployment/business/contracts/in';
import {
  DeleteDeploymentResponse,
  UpdateDeploymentStatusResponse,
  GetDeploymentByIdResponse,
  GetDeploymentsDbResponse,
  GetDeploymentByIdDbResponse,
  DeleteDeploymentDbResponse,
} from '../../../deployment/business/contracts/out';
import { UpdateDeploymentStatusDbResponse } from '../../../deployment/business/contracts/out/UpdateStatusDbResponse';
import { DeploymentDbProps } from '../../../deployment/business/entities/deployment';
import { CombinedDeploymentRepositoriesInterfaces } from '../../../deployment/repository';
import { CombinedScopedDeploymentRepositoriesInterfaces } from '../../../project/repository';
import DeploymentStatuses from '../../../__lib/sharedKernel/deployment/DeploymentStatus';
import paginationService from './pagination';
import TableNames from './tableNamesEnum';

const tableName = TableNames.DEPLOYMENTS;

const create =
  (
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    dbConnection: Knex<any, unknown>
  ) =>
  async (command: CreateDeploymentDbCommand): Promise<DeploymentDbProps> => {
    const deployment = await dbConnection(tableName)
      .insert({
        deployed_in: command.deployed_in,
        status: command.status,
        created_at: command.created_at,
        project_id: command.project_id,
      })
      .returning('*');

    return <DeploymentDbProps>deployment[0];
  };

const getDeployments =
  (
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    dbConnection: Knex<any, unknown>
  ) =>
  async (request: GetDeploymentsRequest): Promise<GetDeploymentsDbResponse> => {
    const { offset, limit } = paginationService(request.page, request.limit);
    const deployments = await dbConnection(tableName).select('*').offset(offset).limit(limit);
    const total = await dbConnection(tableName).count('id');

    const result = {
      data: deployments,
      total: total[0].count,
    };

    return <GetDeploymentsDbResponse>result;
  };

const getDeploymentById =
  (
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    dbConnection: Knex<any, unknown>
  ) =>
  async (id: GetDeploymentByIdRequest): Promise<GetDeploymentByIdDbResponse> => {
    const deployment = await dbConnection(tableName).select('*').where('id', id);

    return <GetDeploymentByIdDbResponse>deployment[0];
  };

const updateStatus =
  (
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    dbConnection: Knex<any, unknown>
  ) =>
  async (command: UpdateDeploymentStatusDbCommand): Promise<UpdateDeploymentStatusDbResponse> => {
    const deployment = await dbConnection(tableName)
      .where('id', command.id)
      .update({
        status: command.status,
        deployed_in: command.deployed_in || null,
      })
      .returning('*');

    return <UpdateDeploymentStatusDbResponse>deployment[0];
  };

const softDelete =
  (
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    dbConnection: Knex<any, unknown>
  ) =>
  async (command: DeleteDeploymentDbCommand): Promise<DeleteDeploymentDbResponse> => {
    const deployment = await dbConnection(tableName)
      .where('id', command.id)
      .update({
        status: DeploymentStatuses.CANCELLED,
      })
      .returning('*');

    return <DeleteDeploymentDbResponse>deployment[0];
  };

const isPrimerDeployment =
  (
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    dbConnection: Knex<any, unknown>
  ) =>
  async (request: UpdateDeploymentStatusCommand): Promise<boolean> => {
    const deployment = await dbConnection(tableName).select('project_id').where('id', request.id);
    const firstDeployment = await dbConnection(tableName)
      .select('*')
      .where('project_id', deployment[0].project_id)
      .orderBy('created_at', 'asc')
      .first();

    return firstDeployment.id === request.id;
  };

const makeDeploymentRepository = (
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  dbConnection: Knex<any, unknown>
): CombinedDeploymentRepositoriesInterfaces & CombinedScopedDeploymentRepositoriesInterfaces => {
  return Object.freeze({
    create: create(dbConnection),
    getDeployments: getDeployments(dbConnection),
    getDeploymentById: getDeploymentById(dbConnection),
    softDelete: softDelete(dbConnection),
    updateStatus: updateStatus(dbConnection),
    isPrimerDeployment: isPrimerDeployment(dbConnection),
  });
};

export default makeDeploymentRepository;

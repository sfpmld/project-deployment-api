import { Knex } from 'knex';
import { LookupDeploymentAppSecretRequest } from '../../../deployment/business/contracts/in';
import { LookupDeploymentAppSecretResponse } from '../../../deployment/business/contracts/out';
import {
  ListProjectsRequest,
  GetProjectByIdRequest,
  UpdateProjectUrlCommand,
} from '../../../project/business/contracts/in';
import {
  GetProjectByIdDbResponse,
  ListProjectsDbResponse,
  UpdateProjectUrlResponse,
} from '../../../project/business/contracts/out';
import { CombinedProjectRepositoriesInterfaces } from '../../../project/repository';
import paginationRequestToKnex from './pagination';
import TableNames from './tableNamesEnum';

const tableName = TableNames.PROJECTS;

const listProjects =
  (
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    dbConnection: Knex<any, unknown>
  ) =>
  async (request: ListProjectsRequest): Promise<ListProjectsDbResponse> => {
    const { offset, limit } = paginationRequestToKnex(request.page, request.limit);
    const projects = await dbConnection(tableName).select('*').offset(offset).limit(limit);
    const totalProjects = await dbConnection(tableName).count('id');

    return { data: projects, total: <number>totalProjects[0].count };
  };

const getProjectById =
  (
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    dbConnection: Knex<any, unknown>
  ) =>
  async (requestId: GetProjectByIdRequest): Promise<GetProjectByIdDbResponse> => {
    const project = await dbConnection(tableName).select('*').where('id', requestId);

    return <GetProjectByIdDbResponse>project[0];
  };

const updateUrl =
  (
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    dbConnection: Knex<any, unknown>
  ) =>
  async (command: UpdateProjectUrlCommand): Promise<UpdateProjectUrlResponse> => {
    const project = await dbConnection(tableName).where('id', command.id).update({
      url: command.url,
    });

    return !!project;
  };

const lookupAppSecret =
  (
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    dbConnection: Knex<any, unknown>
  ) =>
  async (request: LookupDeploymentAppSecretRequest): Promise<LookupDeploymentAppSecretResponse> => {
    const isAuthorized = await dbConnection(tableName)
      .select('id')
      .where('id', request.id)
      .andWhere('app_secret', request.bearerAgainstAppSecret);

    return !!isAuthorized[0];
  };

const makeProjectRepository = (
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  dbConnection: Knex<any, unknown>
): CombinedProjectRepositoriesInterfaces => {
  return Object.freeze({
    listProjects: listProjects(dbConnection),
    getProjectById: getProjectById(dbConnection),
    updateUrl: updateUrl(dbConnection),
    lookupAppSecret: lookupAppSecret(dbConnection),
  });
};

export default makeProjectRepository;

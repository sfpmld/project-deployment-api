enum TableNames {
  PROJECTS = 'projects',
  DEPLOYMENTS = 'deployments',
  USERS = 'users',
}

export default TableNames;

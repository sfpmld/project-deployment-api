import knex from 'knex';
import { ParsedConfig } from '../../../../../__config/index';

const knexPg = (config: ParsedConfig) =>
  knex({
    client: config.client,
    connection: {
      host: config.connection.host,
      port: config.connection.port,
      database: config.connection.database,
      user: config.connection.user,
      password: config.connection.password,
    },
  });

export default knexPg;

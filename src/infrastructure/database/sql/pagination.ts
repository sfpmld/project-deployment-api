import DEFAULT_PAGINATION_LIMIT from '../../../__config/pagination';

const paginationRequestToKnex = (
  page: number,
  limit?: number
): { offset: number; limit: number } => {
  const limitOrDefault = limit || DEFAULT_PAGINATION_LIMIT;
  const offset = (page - 1) * limitOrDefault;

  return { offset, limit: limitOrDefault };
};

export const calculateTotalPages = (total: number, limit?: number) => {
  const limitOrDefault = limit || DEFAULT_PAGINATION_LIMIT;
  const totalPages = Math.ceil(total / limitOrDefault);

  return totalPages;
};

export default paginationRequestToKnex;

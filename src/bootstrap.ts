import { AllUseCasesInterface } from './infrastructure/http-gateway/rest/server';
import makeCreateDeploymentUseCase from './deployment/application/usecases/CreateDeploymentUseCase';
import makeDeleteDeploymentUseCase from './deployment/application/usecases/DeleteDeploymentUseCase';
import makeGetDeploymentByIdUseCased from './deployment/application/usecases/GetDeploymentByIdUseCase';
import makeGetDeploymentsUseCase from './deployment/application/usecases/GetDeploymentsUseCase';
import makeUpdateDeploymentStatusUseCase from './deployment/application/usecases/UpdateDeploymentStatusUseCase';
import makeCreateProjectDeploymentUseCase from './project/application/usecases/createProjectDeployment.usecase';
import makeGetProjectByIdUseCase from './project/application/usecases/getProjectById.usecase';
import makeListProjectUseCase from './project/application/usecases/listProjects.usecase';
import makeUpdateProjectUrlUseCase from './project/application/usecases/updateProjectUrl.usecase';
import { AllDependencies } from './bootstrapDeps';

const bootstrap = (deps: AllDependencies): AllUseCasesInterface => {
  // Deployment Use Cases
  const createDeploymentUseCase = makeCreateDeploymentUseCase(
    deps.deploymentRepository,
    deps.eventBus,
    deps.dateProvider
  );
  const deleteDeploymentUseCase = makeDeleteDeploymentUseCase(
    deps.deploymentRepository,
    deps.eventBus,
    deps.dateProvider
  );
  const getDeploymentByIdUseCase = makeGetDeploymentByIdUseCased(deps.deploymentRepository);
  const getDeploymentsUseCase = makeGetDeploymentsUseCase(deps.deploymentRepository);
  const updateDeploymentStatusUseCase = makeUpdateDeploymentStatusUseCase(
    deps.deploymentRepository,
    deps.projectRepository,
    deps.eventBus,
    deps.urlRandomizer,
    deps.dateProvider
  );

  // Project Use Cases
  const createProjectDeploymentUseCase = makeCreateProjectDeploymentUseCase(deps.deploymentService);
  const getProjectByIdUseCase = makeGetProjectByIdUseCase(
    deps.projectRepository,
    deps.deploymentRepository
  );
  const listProjectsUseCase = makeListProjectUseCase(
    deps.projectRepository,
    deps.deploymentRepository
  );
  const updateProjectUrlUseCase = makeUpdateProjectUrlUseCase(deps.projectRepository);

  const useCases = {
    // Deployment Use Cases
    createDeploymentUseCase,
    deleteDeploymentUseCase,
    getDeploymentByIdUseCase,
    getDeploymentsUseCase,
    updateDeploymentStatusUseCase,
    // Project Use Cases
    createProjectDeploymentUseCase,
    getProjectByIdUseCase,
    listProjectsUseCase,
    updateProjectUrlUseCase,
  };

  return useCases;
};

export default bootstrap;

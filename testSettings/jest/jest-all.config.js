module.exports = {
  displayName: 'Strapi Tech Assignment',
  verbose: true,
  moduleFileExtensions: ['js', 'ts', 'tsx', 'json'],
  rootDir: '../../src',
  testRegex: '.unit-test.ts$|.spec.ts$|.integ-test.ts$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  testEnvironment: 'node',
  testPathIgnorePatterns: ['/node_modules/', '/dist/'],
};

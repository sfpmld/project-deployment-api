import { Knex } from 'knex';
import projectSamples from '../fixtures/projects.json';
import deploymentSamples from '../fixtures/deployments.json';
import userSamples from '../fixtures/users.json';
import TableNames from '../../src/infrastructure/database/sql/tableNamesEnum';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const seedUsers = async (dbConnection: Knex<any, unknown[]>) => {
  await dbConnection(TableNames.USERS).insert(userSamples);
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const seedProjects = async (dbConnection: Knex<any, unknown[]>) => {
  await seedUsers(dbConnection);
  await dbConnection(TableNames.PROJECTS).insert(projectSamples);
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const seedDeployments = async (dbConnection: Knex<any, unknown[]>) => {
  await seedProjects(dbConnection);
  await dbConnection(TableNames.DEPLOYMENTS).insert(deploymentSamples);
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const truncateData = async (dbConnection: Knex<any, unknown[]>) => {
  await dbConnection.raw(`TRUNCATE TABLE ${TableNames.DEPLOYMENTS} CASCADE`);
  await dbConnection.raw(`TRUNCATE TABLE ${TableNames.PROJECTS} CASCADE`);
  await dbConnection.raw(`TRUNCATE TABLE ${TableNames.USERS} CASCADE`);
};

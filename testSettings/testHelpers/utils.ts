const generatefakeDoneDeploymentDateISOString = (date: Date, plusSec: number) => {
  const plusMilli = plusSec * 1000;
  const datePlus = new Date(date.getTime() + plusMilli);

  return datePlus.toISOString();
};

export default generatefakeDoneDeploymentDateISOString;
